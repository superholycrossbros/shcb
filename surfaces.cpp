/* Natalie Sanders, Katie Weiss, Kendra Bilardello
   surfaces.cpp
   Class for the management of loading, blitting, and initializing SDL surfaces
*/

#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "SDL/SDL_rotozoom.h"
#include <iostream>
#include "surfaces.h"

//Constructor for this class does not need to initalize any data
Surfaces::Surfaces()
{
}

//Loading the image onto the screen
SDL_Surface* Surfaces::load_files( string filename, float scale )
{
    SDL_Surface* loadedImage = NULL;
    SDL_Surface* optimizedImage = NULL;
    SDL_Surface* scaledImage = NULL;
		
    loadedImage = IMG_Load( filename.c_str() );

    //If the image loaded
    if( loadedImage != NULL )
    {
        //Create an optimized surface
        optimizedImage = SDL_DisplayFormatAlpha( loadedImage );
        scaledImage = rotozoomSurface( optimizedImage, 0., scale, 1);

        //Free the old surface
        SDL_FreeSurface( loadedImage );
        SDL_FreeSurface( optimizedImage );
    }
    else
    {
        cout << "error" << endl;
    }

    return scaledImage;
}


//Applying an image onto the screen. This is used for text, enemies, main_characters, blocks etc.
void Surfaces::apply_surface( int x, int y, SDL_Surface* &source, SDL_Surface* &destination, SDL_Rect* clip )
{
    //Holds offsets
    SDL_Rect offset;

    //Get offsets
    offset.x = x;
    offset.y = y;

    //Blit image to the screen
    if(source != NULL && destination != NULL){
        SDL_BlitSurface( source, clip, destination, &offset );
    }else{
			  cout << "NULL" << endl;
		}
}


//Initialzes the Surfaces
bool Surfaces::init()
{
    //Initialize all SDL subsystems
    if( SDL_Init( SDL_INIT_EVERYTHING ) == -1 ){
        return false;
    }

    //Set up the screen
    screen = SDL_SetVideoMode( SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BPP, SDL_SWSURFACE );

    //If there was an error in setting up the screen
    if( screen == NULL ){
        return false;
    }

    SDL_WM_SetCaption( "Super Holy Cross Bros.", NULL ); //Window caption

    return true;
}

