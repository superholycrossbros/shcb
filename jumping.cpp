/*This code has been created by editing the code given by Lazy Foo tutorials.

This source code copyrighted by Lazy Foo' Productions (2004-2013)
and may not be redistributed without written permission.*/

//The headers
#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include <string>
#include <iostream>

//Screen attributes
const int SCREEN_WIDTH = 909;
const int SCREEN_HEIGHT = 600;
const int SCREEN_BPP = 32;

//The frames per second
const int FRAMES_PER_SECOND = 10;

//The dimenstions of the stick figure
const int FOO_WIDTH = 64;
const int FOO_HEIGHT = 205;

//The direction status of the stick figure
const int FOO_RIGHT = 0;
const int FOO_LEFT = 1;


//The surfaces
SDL_Surface *foo = NULL;
SDL_Surface *screen = NULL;
SDL_Surface *background = NULL;

//The event structure
SDL_Event event;

//The areas of the sprite sheet
SDL_Rect clipsRight[ 4 ];
SDL_Rect clipsLeft[ 4 ];

//The stick figure
class Foo
{
   public:
    Foo();
    void handle_events(); //Deals with key presses
    void move(int); //Changes velocity of figure
    int show(int, int); //Displays figure on screen

   private:
    int x, y; //x and y position on screen
    int xVel, yVel; //x and y velocity
    int frame; //Current frame
    int status;//Current animation status
};

//The timer
class Timer
{
   public:
   	Timer();
    void start(); //Starts the timer
    int get_ticks(); //Returns current ticks-start ticks
    bool is_started();

   private:
    int startTicks; //Clock time when clock starts
    bool started;

};


//Loading the image onto the screen
SDL_Surface *load_image( std::string filename )
{
    SDL_Surface* loadedImage = NULL;
    SDL_Surface* optimizedImage = NULL;

    loadedImage = IMG_Load( filename.c_str() );

    //If the image loaded
    if( loadedImage != NULL )
    {
        //Create an optimized surface
        optimizedImage = SDL_DisplayFormat( loadedImage );

        //Free the old surface
        SDL_FreeSurface( loadedImage );

        //If the surface was optimized
        if( optimizedImage != NULL )
        {
            //Color key surface
            SDL_SetColorKey( optimizedImage, SDL_SRCCOLORKEY, SDL_MapRGB( optimizedImage->format, 0, 0xFF, 0xFF ) );
        }
    }

    return optimizedImage;
}

void apply_surface( int x, int y, SDL_Surface* source, SDL_Surface* destination, SDL_Rect* clip = NULL )
{
    //Holds offsets
    SDL_Rect offset;

    //Get offsets
    offset.x = x;
    offset.y = y;

    //Blit
    SDL_BlitSurface( source, clip, destination, &offset );
}

void set_clips()
{
    //Clip the sprites
    clipsRight[ 0 ].x = 0;
    clipsRight[ 0 ].y = 0;
    clipsRight[ 0 ].w = FOO_WIDTH;
    clipsRight[ 0 ].h = FOO_HEIGHT;

    clipsRight[ 1 ].x = FOO_WIDTH;
    clipsRight[ 1 ].y = 0;
    clipsRight[ 1 ].w = FOO_WIDTH;
    clipsRight[ 1 ].h = FOO_HEIGHT;

    clipsRight[ 2 ].x = FOO_WIDTH * 2;
    clipsRight[ 2 ].y = 0;
    clipsRight[ 2 ].w = FOO_WIDTH;
    clipsRight[ 2 ].h = FOO_HEIGHT;

    clipsRight[ 3 ].x = FOO_WIDTH * 3;
    clipsRight[ 3 ].y = 0;
    clipsRight[ 3 ].w = FOO_WIDTH;
    clipsRight[ 3 ].h = FOO_HEIGHT;

    clipsLeft[ 0 ].x = 0;
    clipsLeft[ 0 ].y = FOO_HEIGHT;
    clipsLeft[ 0 ].w = FOO_WIDTH;
    clipsLeft[ 0 ].h = FOO_HEIGHT;

    clipsLeft[ 1 ].x = FOO_WIDTH;
    clipsLeft[ 1 ].y = FOO_HEIGHT;
    clipsLeft[ 1 ].w = FOO_WIDTH;
    clipsLeft[ 1 ].h = FOO_HEIGHT;

    clipsLeft[ 2 ].x = FOO_WIDTH * 2;
    clipsLeft[ 2 ].y = FOO_HEIGHT;
    clipsLeft[ 2 ].w = FOO_WIDTH;
    clipsLeft[ 2 ].h = FOO_HEIGHT;

    clipsLeft[ 3 ].x = FOO_WIDTH * 3;
    clipsLeft[ 3 ].y = FOO_HEIGHT;
    clipsLeft[ 3 ].w = FOO_WIDTH;
    clipsLeft[ 3 ].h = FOO_HEIGHT;
}

bool init()
{
    //Initialize all SDL subsystems
    if( SDL_Init( SDL_INIT_EVERYTHING ) == -1 )
    {
        return false;
    }

    //Set up the screen
    screen = SDL_SetVideoMode( SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BPP, SDL_SWSURFACE );

    //If there was an error in setting up the screen
    if( screen == NULL )
    {
        return false;
    }

    SDL_WM_SetCaption( "Lab 7/8", NULL ); //Window caption

    return true;
}

bool load_files()
{
    //Load the sprite sheet
    foo = load_image( "foo.png" );

		background = load_image( "cloud.png");

    //If there was a problem in loading the sprite
    if( foo == NULL )
    {
      return false;
    }

		//If there was a problem in loading the background
		if ( background == NULL)
		{
			return false;
		}

    return true;
}

void clean_up()
{
    //Free the surface
    SDL_FreeSurface( foo );
		SDL_FreeSurface(background);

    //Quit SDL
    SDL_Quit();
}


//Foo default constructor that initializes the location and appearance of the foo image
Foo::Foo()
{
    //Initialize animation variables
    frame = 0;
    status = FOO_RIGHT;

    //Initialize the offsets to bottom left of screen
    x = 0;
    y = 900;

    //Initialize the velocity
    xVel = 0;
    yVel = 0;

}

//Deals with the key presses: left and right arrow and space to jump
void Foo::handle_events()
{
     //If a key was pressed
    if( event.type == SDL_KEYDOWN )
    {
        //Change the velocity
        switch( event.key.keysym.sym )
        {
            case SDLK_LEFT: xVel -= FOO_WIDTH / 2; break;
            case SDLK_RIGHT: xVel += FOO_WIDTH / 2; break;
						case SDLK_SPACE: yVel -=FOO_HEIGHT /2; break;
        }
    }
    //If a key was released
    else if( event.type == SDL_KEYUP )
    {
        //Change velocity
        switch( event.key.keysym.sym )
        {
            case SDLK_LEFT: xVel += FOO_WIDTH / 2; break;
            case SDLK_RIGHT: xVel -= FOO_WIDTH / 2; break;
						case SDLK_SPACE: yVel +=FOO_HEIGHT /2 ; break;
        }
    }


}

//Moves the foo to a different location on the screen
void Foo::move(int time)
{
    //Move the figure left or right
    x += xVel;

    //If the figure went too far to the left or right
    if( ( x < 0 ) || ( x + FOO_WIDTH > SCREEN_WIDTH ) )
    {
        //Move back
        x -= xVel;
    }

    //Move the figure up or down
		int ground=550;
 		y+= yVel;

		//If figure is not on ground apply "gravity"
		if (y + FOO_HEIGHT < ground )
		{
				yVel+=3;
		}
		else //If it is on ground the yVel is zero
		{
			yVel=0;
			y=ground-FOO_HEIGHT;

		}

    //If the figure went too far up or down
    if( ( y < 0 ) || ( y + FOO_HEIGHT > SCREEN_HEIGHT ) )
    {
        //move back
        y -= yVel;
    }

}

int Foo::show(int bgX, int bgY)
{

    //If Foo is moving left
    if( xVel < 0 )
    {
        //Set the animation to left
        status = FOO_LEFT;

        //Move to the next frame in the animation
        frame++;

				bgX +=20;

				if (bgX<= -background ->w)
				{
						bgX=0;
				} 

				apply_surface(bgX,bgY, background, screen);
				apply_surface (bgX+background->w, bgY, background, screen);

    }
    //If Foo is moving right
    else if( xVel > 0 )
    {
        //Set the animation to right
        status = FOO_RIGHT;

        //Move to the next frame in the animation
        frame++;

				bgX -=20;

				if (bgX<= -background ->w)
				{
						bgX=0;
				}
				apply_surface(bgX,bgY, background, screen);
				apply_surface (bgX+background->w, bgY, background, screen);

    }
    //If Foo standing
    else
    {
        //Restart the animation
        frame = 0;
				apply_surface(bgX,bgY, background, screen);
				apply_surface (bgX+background->w, bgY, background, screen);
    }

    //Loop the animation
    if( frame >= 4 )
    {
        frame = 0;
    }

    //Show the stick figure
    if( status == FOO_RIGHT )
    {
        apply_surface( x, y, foo, screen, &clipsRight[ frame ] );
    }
    else if( status == FOO_LEFT )
    {
        apply_surface( x, y, foo, screen, &clipsLeft[ frame ] );
    }


	return bgX;
}

Timer::Timer()
{
    //Initialize the variables
    startTicks = 0;
    started = false;
}

void Timer::start()
{
    //Start the timer
    started = true;

    //Get the current clock time
    startTicks = SDL_GetTicks();
}

int Timer::get_ticks()
{
    //If the timer is running
    if( started == true )
    {
        //Return the current time minus the start time
        return SDL_GetTicks() - startTicks;

    }

    //If the timer isn't running
    return 0;
}

bool Timer::is_started()
{
    return started;
}


int main( int argc, char* args[] )
{
 SDL_Surface* background = NULL;


    //Quit flag
    bool quit = false;

		//Offsets of background
		int bgX=0, bgY=0;

    //Initialize
    if( init() == false )
    {
        return 1;
    }

    //Load the files
    if( load_files() == false )
    {
        return 1;
    }

    //Clip the sprite sheet
    set_clips();

    //The frame rate regulator
    Timer fps;

    //The stick figure
    Foo walk;

	 //Set up screen 
	screen = SDL_SetVideoMode( 909, 600, 32, SDL_SWSURFACE ); 
	//Load image 
	background = load_image( "cloud.png" );


    //While the user hasn't quit
    while( quit == false )
    {
        //Start the frame timer
        fps.start();
				int time=fps.get_ticks();

        //While there's events to handle
        while( SDL_PollEvent( &event ) )
        {
            //Handle events for the stick figure
            walk.handle_events();

            //If the user has Xed out the window
            if( event.type == SDL_QUIT )
            {
                //Quit the program
                quit = true;
            }

					apply_surface(0,0, background, screen);

				 //Update Screen 
					SDL_Flip( screen ); 


        }


        //Move the stick figure
        walk.move(time);

        //Fill the screen with background
				//apply_surface(bgX,bgY, background, screen);
				//apply_surface (bgX+background->w, bgY, background, screen);
				SDL_Flip(screen);

        //Show the stick figure on the screen
        bgX=walk.show(bgX,bgY);

	

        //Update the screen
        if( SDL_Flip( screen ) == -1 )
        {
            return 1;
        }

        //Cap the frame rate
        if( fps.get_ticks() < 1000 / FRAMES_PER_SECOND )
        {
            SDL_Delay( ( 1000 / FRAMES_PER_SECOND ) - fps.get_ticks() );
        }
    }

    //Clean up
    clean_up();

    return 0;
}
