/* Natalie Sanders, Katie Weiss, Kendra Bilardello
   home_screen.h
   Class for the images on the home/main_screen
	 This class creates the messages/text based comments that need to be displayed onto the screen. This includes the main option screen, the character option screen, the level transition screens, and the ending screens.
*/


#ifndef HOME_SCREEN_H
#define HOME_SCREEN_H

#include "constants.h"
#include "surfaces.h"
#include "SDL/SDL_ttf.h"
#include <sstream>
#include <string.h>

using namespace std;

extern SDL_Surface *screen;
extern SDL_Surface *background;
extern int LIVES;
extern double POINTS;

class home_screen : public Surfaces
{
	public:
		home_screen(); //Initializes message values to null
		~home_screen(); //Cleans up the data
		void display_main(Surfaces &, int); //Displays messages on screen 
		void set_messages(int); //Sets the messages to correct values
		void set_character_image(Surfaces &); //Clips image
		void display_character(Surfaces &); // Displays character screen
		void display_next_level(Surfaces &, int); //Displays level num
		void end_level(Surfaces &); //Congrats you won screen
		void lost(Surfaces &); //Sorry you lost screen

	private:

		SDL_Surface *start_screen_graphic; //Colorful start screen
		SDL_Surface *start_message; //Start game option
		SDL_Surface *quit_message; //Quit game option
		SDL_Surface *character_message; //Change character option

		SDL_Surface *leprechaun_message; //Pick leprechaun option
		SDL_Surface *Scotty_message; //Pick Scotty option
		SDL_Surface *char_option1; //Displays leprechaun sprite
		SDL_Surface *char_option2; //Displays Scotty sprite

		//Displays level num when transitioning through levels
		SDL_Surface *level1_message;
		SDL_Surface *level2_message;
		SDL_Surface *level3_message;
		SDL_Surface *level4_message;
		SDL_Surface *level5_message;

		SDL_Surface *end_message; //Winning message
		SDL_Surface *lost_message; //Losing message
		SDL_Surface *reset_message; //Reset option message

		SDL_Surface *score_message; //Shows score label
		SDL_Surface *score_value; //Show score on screen
		SDL_Surface *lives_message; //Shows lives label
		SDL_Surface *lives_value; //Show lives on screen

		TTF_Font *font; //Loading font
		SDL_Rect clips[1]; //Clip sheet for displaying char on char option


};


#endif
