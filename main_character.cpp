/* Natalie Sanders, Katie Weiss, Kendra Bilardello
   main_character.cpp
   Class for the movement of a character on the screen
*/

#include "main_character.h"
#include "surfaces.h"
#include "tile.h"
#include <vector>


//Foo default constructor that initializes the location and appearance of the foo image
main_character::main_character (string charFile):jump ("jump.wav", " ")
{
  //Initialize animation variables
  frame = 0;		// the clip number
  status = FOO_RIGHT;	// status indicates the direction of current movement

  //Initialize the offsets to bottom left of screen
  coord.x = 0;
  coord.y = LEVEL_HEIGHT;

  //Initialize the velocity
  vel.x = 0;
  vel.y = 0;

  //Load character file and make clips
  mainC = load_files (charFile, SCALE);
  set_clips ();

  currentBlock = -1;	// This variable is used in determining collision detection
  wall_height = 0;	// This variable is used in determining collision detection 

  boss_hit = 0;		// This variable is used to return to typical motion after character has bounced off of boss's top
  key_up = 0;		// This variable is used to return to typical motion after character has bounced off of boss's top

}

main_character::~main_character ()
{
  SDL_FreeSurface (mainC); // free characters SDL_Surface
  jump.clean_up ();	   // free jump sound 
}


void
main_character::set_clips ()
{
  //Character walking to the right
  clipsRight[0].x = 0;
  clipsRight[0].y = 0;
  clipsRight[0].w = FOO_WIDTH;
  clipsRight[0].h = FOO_HEIGHT;

  clipsRight[1].x = FOO_WIDTH;
  clipsRight[1].y = 0;
  clipsRight[1].w = FOO_WIDTH;
  clipsRight[1].h = FOO_HEIGHT;

  clipsRight[2].x = FOO_WIDTH * 2;
  clipsRight[2].y = 0;
  clipsRight[2].w = FOO_WIDTH;
  clipsRight[2].h = FOO_HEIGHT;

  clipsRight[3].x = FOO_WIDTH * 3;
  clipsRight[3].y = 0;
  clipsRight[3].w = FOO_WIDTH;
  clipsRight[3].h = FOO_HEIGHT;

  clipsRight[4].x = FOO_WIDTH * 4;
  clipsRight[4].y = 0;
  clipsRight[4].w = FOO_WIDTH;
  clipsRight[4].h = FOO_HEIGHT;

  clipsRight[5].x = FOO_WIDTH * 5;
  clipsRight[5].y = 0;
  clipsRight[5].w = FOO_WIDTH;
  clipsRight[5].h = FOO_HEIGHT;

  //Character jumping to the right
  clipsRight[6].x = FOO_WIDTH * 6;
  clipsRight[6].y = 0;
  clipsRight[6].w = FOO_WIDTH;
  clipsRight[6].h = FOO_HEIGHT;

  //Character walking to the left
  clipsLeft[0].x = 0;
  clipsLeft[0].y = FOO_HEIGHT;
  clipsLeft[0].w = FOO_WIDTH;
  clipsLeft[0].h = FOO_HEIGHT;

  clipsLeft[1].x = FOO_WIDTH;
  clipsLeft[1].y = FOO_HEIGHT;
  clipsLeft[1].w = FOO_WIDTH;
  clipsLeft[1].h = FOO_HEIGHT;

  clipsLeft[2].x = FOO_WIDTH * 2;
  clipsLeft[2].y = FOO_HEIGHT;
  clipsLeft[2].w = FOO_WIDTH;
  clipsLeft[2].h = FOO_HEIGHT;

  clipsLeft[3].x = FOO_WIDTH * 3;
  clipsLeft[3].y = FOO_HEIGHT;
  clipsLeft[3].w = FOO_WIDTH;
  clipsLeft[3].h = FOO_HEIGHT;

  clipsLeft[4].x = FOO_WIDTH * 4;
  clipsLeft[4].y = FOO_HEIGHT;
  clipsLeft[4].w = FOO_WIDTH;
  clipsLeft[4].h = FOO_HEIGHT;

  clipsLeft[5].x = FOO_WIDTH * 5;
  clipsLeft[5].y = FOO_HEIGHT;
  clipsLeft[5].w = FOO_WIDTH;
  clipsLeft[5].h = FOO_HEIGHT;

  // Character jumping to left
  clipsLeft[6].x = FOO_WIDTH * 6;
  clipsLeft[6].y = FOO_HEIGHT;
  clipsLeft[6].w = FOO_WIDTH;
  clipsLeft[6].h = FOO_HEIGHT;
}


//Deals with the key presses: left and right arrow and space to jump
void
main_character::handle_events (int level)
{
  int loop = 0;
  //If a key was pressed
  if (event.type == SDL_KEYDOWN)
    {
      //Change the velocity
      switch (event.key.keysym.sym)
	{
	case SDLK_LEFT:	//for the left arrow key
	  if (coord.x == 0 && vel.x < 0) // if the character is trying to walk of the left of screen
	    {
	      vel.x = 0;
	    }
	  else if (boss_hit == 1)        // If this is the first time a button's been pressed since the character jumped on the boss
	    {
	      vel.x = -FOO_WIDTH / 7;
	      boss_hit = 0;
              key_up = 0;
            }
	  else
	    {
	      vel.x = -FOO_WIDTH / 7;	//move character to left
	    }
	  loop = 1;
	  break;

	case SDLK_RIGHT: //for the right arrow key
	  if (coord.x == 0 && vel.x > 0)
	    {
	      vel.x = 0;
	    }
	  else if (boss_hit == 1)	// If this is the first time a button's been pressed since the character jumped on the boss
	    {
	      vel.x = FOO_WIDTH / 7;
	      boss_hit = 0;
              key_up = 0;
            }
	  else
	    {
	      vel.x = FOO_WIDTH / 7;	//move character to the right 
	    }
	  loop = 1;
	  break;
	case SDLK_SPACE: //for space bar
	  if (vel.y == 0)		// check that the character is not currently jumping
	    {
	      vel.y -= FOO_HEIGHT / 4.2;	//jump (move character up)
	      jump.play ();
	    }
	  break;
	}
    }
  //If a key is released (that had been pressed while boss_hit = 1, in level 5)
  else if (event.type == SDL_KEYUP && !key_up) 
    {
      //Change velocity
      switch (event.key.keysym.sym)
	{
	case SDLK_LEFT: // left arrow
  	  //Cannot move left at beginning of level
	  //Will not animate if you try
	  if (coord.x == 0 && vel.x > 0)
	    {
	      vel.x = 0;
	    }
	  else if (vel.x < 0)
	    {
	      vel.x = 0 ;	//Stop character moving left
	    }
	  break;


	case SDLK_RIGHT: // right arrow
	  if (coord.x == 0 && vel.x < 0)
	    {
	      vel.x = 0;
	    }
	    else if (vel.x > 0 )
	    {
	      vel.x = 0;		//Stop character moving right
	    }

	  break;

	case SDLK_SPACE: // space bar
	  vel.y += FOO_HEIGHT / 6;	// bring character back down
	  break;
	}
    }
  else if ( event.type == SDL_KEYUP && key_up) // first time a key is released after character hit boss
    {
        key_up = 0;
    }

  if (coord.x == 0 && loop == 0)
    {
      vel.x = 0;
    }

}


// displays main character on screen while calling functions to check for collisions
void main_character::show (tile & T, Enemy & E, boss & B, Shamrock & S)
{
	  S.check_collision (coord, T.getTILES ()); // check collisions with shamrocks

	  if (vel.x < 0)		//If character is moving right
	    {
      	      //Set the animation to left
	      status = FOO_LEFT;
	      frame++;
	
	    }
	  else if (vel.x > 0)		//If character is moving left
	    {
	      //Set the animation to right
	      status = FOO_RIGHT;

	      //Move to the next frame in the animation
	      frame++;

	    }
  	else				//If character is not moving
	    {
	      frame = 0;		//Restart animation
	    }
	
	
	  // Loop the animation (go though the stages of walking)
	  if (frame >= 6)
	    {
	      frame = 0;
	    }
	
	  // If the character is jumping use the jump sprite
	  if (vel.y != 0)
	    {
	      frame = 6;
	    }
	
	  //Apply the correct image of the character to the screen
	  if (status == FOO_RIGHT)
	    {
	      apply_surface (coord.x - camera.x, coord.y - camera.y, mainC, screen,
			     &clipsRight[frame]);
	
	
	    }
	
	  else if (status == FOO_LEFT)
	    {
	      apply_surface (coord.x - camera.x, coord.y - camera.y, mainC, screen,
			     &clipsLeft[frame]);
	    }
	
	
	  E.check_collision (coord); // check collision with enemy
	  B.check_collision (coord); // check collision with boss
	
}




//Moves the foo to a different location on the screen
int main_character::move (tile & Tiles, bool end, Enemy & Enemies, boss & bosses,
		      Shamrock & shamrocks)
{
  shamrocks.check_collision (coord, Tiles.getTILES ());

  // MOVE LEFT/RIGHT //
/////////////////////////
  coord.x += vel.x;


  // Check for collisions with edge of screen or with the L/R side of a block
  for (int i = 0; i < TOTAL_TILES; i++)
    {
      //If the figure went too far to the left or right
      if ((coord.x < 0) || (coord.x + FOO_WIDTH > LEVEL_WIDTH) || 
	  (Tiles.check_side_collision (coord, vel, i, &wall_height, &currentBlock)))
	{
	  //Move back
	  coord.x -= vel.x;

	  if(boss_hit == 1) 
          {
              vel.x = 0;
              vel.y = 0;

	      /* Indicates that a collision has occured since the char/boss encounter
	      Used to regulate the velocity as the character bounces back*/
	      boss_hit = 0;
	      key_up = 0;
 	  }

	}
    }

   // MOVE UP/DOWN //
//////////////////////////

  int ground = SCREEN_HEIGHT - 100;
  coord.y += vel.y;

  //If the figure is not on ground and he hasn't reached terminal velocity...
  if (coord.y + FOO_HEIGHT < ground - wall_height && vel.y <= 10)
    {
      vel.y += 2; // apply "gravity"
    }
  //If the figure is not on ground and he has reached terminal velocity...
  else if (coord.y + FOO_HEIGHT < ground - wall_height && vel.y > 10)
    {
      vel.y += 0;
    }
  // Otherwise it is on ground/block and the vel.y is 0
  else
    {
      // ...and if he just came from jumping on top of the boss, the vel.x is also 0
      if (boss_hit == 1)
       {
         vel.y = 0;
	 vel.x = 0;

	 boss_hit = 0;
	 key_up = 0; 
       }

      vel.y = 0;
      coord.y = ground - FOO_HEIGHT - wall_height;
    }




  // Check for collisions with edge of screen or with the T/B side of a block
  for (int i = TOTAL_TILES - 1; i >= 0; i--)
    {
      SDL_Rect temp;
      temp = Tiles.getTILES ()[i];
      //If the figure went too far up
      if (coord.y < 0 || (coord.y + FOO_HEIGHT > SCREEN_HEIGHT) || 
	  (Tiles.check_topbot_collision (coord, vel, i, &wall_height, &currentBlock)))
	{
	  //move back

	  // If the character was coming down...
	  if (vel.y > 0)
	    {
	      // ...and the character hit the top of the screen
	      if (coord.y < 0)
		{
		  vel.y += 2;
		  coord.y += vel.y;
		}
	      // ...and he's landed on a block
	      else
		{

		  coord.y = ground - FOO_HEIGHT - wall_height;
		}
	    }

    	  // If he just came from jumping on top of the boss
          if (boss_hit == 1)
          {
            vel.y = 0;
	    vel.x = 0;
	    boss_hit = 0;
	    key_up = 0;
	  }

	  vel.y = 0;
	  break;

	}
    }

  // Check for collision with enemy
  if (Enemies.check_collision (coord))
    {
      coord.y -= vel.y;
    }
  // Check for collision with boss
  if (bosses.check_collision (coord))
    {
      key_up   = 1;
      boss_hit = 1;
      coord.y -= 50;
      coord.x -= 25;

      vel.x = -5;
      vel.y = -6; 
    }

  //The character has hit the end of the level
  if (coord.x >= 1215)
    {
      end = 1;
    }

  return end;
}
