/* Natalie Sanders, Katie Weiss, Kendra Bilardello
   tile.h
   Class for putting tiles (blocks) on screen
*/

#include "tile.h"
#include "surfaces.h"
#include <fstream>
#include <sstream>

using namespace std;

//Constructor for tile class will read in a file of x and y positions and will create two vectors to store this data in order to place the tiles on the screen
tile::tile(char *file_name)
{
    FILE *f = fopen(file_name, "r"); // Opens the input file and creates a file pointer
    if(!f) 			     // Prints error message if the file could not be opened
   	{
        cout << "Could not open file" <<endl;
    }
    else
    {
        char line[100];
        int j = 0;
        while(fgets(line, sizeof(line), f)) // Takes one line from the file at a time
        {
	    //Creates a temporary vector to store the tile values
            SDL_Rect temp;
            temp.x = atoi(line);
            temp.y = atoi(&line[5]);
            temp.w = TILE_WIDTH;
            temp.h = TILE_HEIGHT;

            TILES.push_back(temp); // Puts current block's coordinates into vector of SDL_Rects

	    // A temporary int to store 1/0 to indicate whether the current block is breakable
            int temp_int;
            temp_int = atoi(&line[9]);
            breakable.push_back(temp_int); // Puts current block's breakable indicator into a vector of ints
        }
        fclose(f);
    }  


    TOTAL_TILES = TILES.size();

    //Loops through all bricks to create vector of SDL_Surface pointers to the brick image
    for(int i = 0; i < TOTAL_TILES; i++)
    {
        SDL_Surface *temp = NULL;
        temp = load_files("brick.png", .03);
        if(temp != NULL)
        {
            tileSheet.push_back(temp);
        }
        
    }
 
    puff = load_files("exploding_brick.png", 1); // loads breaking block puff picture
    set_clips(); //Set clips for the tile
}

//Deconstructor for tile class will loop through all the tiles in the tileSheet and free them when the tile goes out of scope
tile::~tile()
{
    for(int i = 0; i < tileSheet.size(); i++)
    {
        SDL_FreeSurface(tileSheet[i]);
    }

    SDL_FreeSurface(puff);
		
}

//Loops through the tilesheet vector to place all the tiles on the screen. This does take some time so the loading of the levels will sometimes take a couple seconds
void tile::apply_tileSurfaces(int x)
{
    //If an enemy was erased, apply_puff=1
    if (apply_puff==1)
    {
        apply_surface(puff_coord.x, puff_coord.y, puff, screen);
    }

    if (TIME<time_tile-1) //Sets length of time to display the puff
    {
        apply_puff=0;
    }
    // applies all tiles
    for (int i=0; i < tileSheet.size(); i++){   
       apply_surface(TILES[i].x - camera.x-x, TILES[i].y - camera.y, tileSheet[i], screen, &clip);
    }

}

bool tile::check_topbot_collision(SDL_Rect coord, SDL_Rect vel, int blockNum, int *wall_height, int *currentBlock)
{
    SDL_Rect B      = TILES[blockNum];

    // The current positions of the character's sides 
    int currLeft,  prevLeft;        
    int currRight, prevRight;
    int currTop,   prevTop;
    int currBot,   prevBot;

    //The sides of the block
    int leftB;
    int rightB;
    int topB;
    int bottomB;



    // Calculate the current sides of the character
    currLeft  = coord.x-camera.x + 15;
    currRight = coord.x + FOO_WIDTH-camera.x - 15;
    currTop   = coord.y-camera.y;
    currBot   = coord.y + FOO_HEIGHT-camera.y-10;

    // Calculate the previous sides of the character
    prevTop   = currTop - vel.y;
    prevBot   = currBot - vel.y;
    prevLeft  = currLeft - vel.x;
    prevRight = currRight - vel.x;

    //Calculate the sides of Rect B, the object
    leftB   = B.x-camera.x-coord.x;// + 15;
    rightB  = B.x + B.w-camera.x-coord.x;// - 15;
    topB    = B.y-camera.y;
    bottomB = B.y + B.h-camera.y;

    // top of character collides w/ bottom of block
    if( ((prevTop >= bottomB && currTop <= bottomB) || (currTop >= bottomB-20 && currBot <= bottomB)) &&
        ((currLeft <= rightB && currRight >= leftB) || (currLeft <= leftB && currRight >= leftB)) && currBot > bottomB) 
    {
        float currMid = (currRight - currLeft);
        if(breakable[blockNum])
        {
     	    //Sets data to display the puff after tile is broken
            apply_puff=1;
            puff_coord.x= TILES[blockNum].x-camera.x-coord.x;
            puff_coord.y= TILES[blockNum].y-camera.y;

	    //Remove tile from SDL_Surface vector and SDL_Rect vector
            SDL_FreeSurface(tileSheet[blockNum]);
            tileSheet.erase(tileSheet.begin()+blockNum);
	    TILES.erase(TILES.begin()+blockNum);
            breakable.erase(breakable.begin()+blockNum);

	    time_tile=TIME;
	    TOTAL_TILES--;
         }
        return true;
    }
 
    // bottom of character collides w/ top of block
    if( ((prevBot <= topB && currBot >= topB) || (currBot <= (topB+20) && currBot >= topB)) &&
        ((currLeft <= rightB && currRight >= leftB) || (currLeft <= leftB && currRight >= leftB)) && currTop < topB) 
    {
        *currentBlock = blockNum;
        *wall_height = (SCREEN_HEIGHT-100) - (B.y + 6);
        return true;
    }
    // If character has jumped off currentBlock
    else if (*currentBlock == blockNum && ((currLeft >= rightB + 3) || (currRight <= leftB - 3)) )
    {
        *wall_height = 0;
        *currentBlock = -1;
    }
 

    // Otherwise  top/botom collision detected
    return false;
}

bool tile::check_side_collision(SDL_Rect coord, SDL_Rect vel, int blockNum, int *wall_height, int *currentBlock)
{
    SDL_Rect B = TILES[blockNum];

    // The current positions of the character's sides 
    int currLeft,  prevLeft;        
    int currRight, prevRight;
    int currTop,   prevTop;
    int currBot,   prevBot;

    //The sides of the block
    int leftB;
    int rightB;
    int topB;
    int bottomB;

    // Calculate the current sides of the character
    currLeft  = coord.x-camera.x + 15;
    currRight = coord.x + FOO_WIDTH-camera.x -15;
    currTop   = coord.y-camera.y;
    currBot   = coord.y + FOO_HEIGHT-camera.y;

    // Calculate the previous sides of the character
    prevTop   = currTop - vel.y;
    prevBot   = currBot - vel.y;
    prevLeft  = currLeft - vel.x;
    prevRight = currRight - vel.x;

    

    //Calculate the sides of Rect B, the object
    leftB   = B.x-camera.x-coord.x;
    rightB  = B.x + B.w-camera.x-coord.x;
    topB    = B.y-camera.y;
    bottomB = B.y + B.h-camera.y;

    // left of character collides w/ right of block
    if(((prevLeft >= rightB && currLeft <= rightB) || (currLeft >= (rightB-20) && currLeft <= rightB)) &&
       ((currTop >= (topB-20) && currTop <= bottomB) || (currBot >= (topB+20) && currBot <= bottomB) || (currBot >= bottomB && currTop <= topB)) ) 
    {
        return true;
    }   
    // right of character collides w/ left of block
    else if(((prevRight <= leftB && currRight >= leftB) || (currRight <= (leftB+20) && currRight >= leftB)) &&
            ((currTop >= (topB-20) && currTop <= bottomB) || (currBot >= (topB+20) && currBot <= bottomB) || (currBot >= bottomB && currTop <= topB)) ) 
    {
        return true;
    }

    // Otherwise no sides overlap
    return false;
}

//Returns the TILES, vector of SDL_Rects
vector< SDL_Rect > tile::getTILES()
{
    return TILES;
}

//Sets the clip for the tile which is just the size of the tile
void tile::set_clips()
{
    clip.x = 0;
    clip.y = 0;
    clip.w = TILE_WIDTH;
    clip.h = TILE_HEIGHT;
}



