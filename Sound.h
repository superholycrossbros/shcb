/* Kendra Bilardello, Natalie Sanders, Katie Weiss
   Sound.h
   Class for playing sounds/music
*/

#ifndef SOUND_H
#define SOUND_H

#include "SDL/SDL.h"
#include "SDL/SDL_mixer.h"
#include <string>

using namespace std;

class Sound
{
    public:
        Sound(string, string);  // constructor takes: name of sound file, name of music file (wav)
        void clean_up();	// halts/frees music, if any; frees sound, if any
        void play();		// play sound/music	
        void stop();		// stop music

    private:
	Mix_Music *music;	// music pointer for music wav file
        Mix_Chunk *sound;	// chunk pointer for sound wav file

};

#endif

