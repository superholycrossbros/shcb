/* Natalie Sanders, Kendra Bilardello, Katie Weiss
   boss.h
   The interface for the class boss
*/

#ifndef BOSS_H
#define BOSS_H

#include "constants.h"
#include "surfaces.h"
#include <vector>

using namespace std;

extern SDL_Surface *screen;
extern SDL_Surface *background;
extern SDL_Rect camera;
extern int TOTAL_ENEMIES;
extern int BOSS_HIT_COUNT;
extern double POINTS;
extern int LIVES;
extern int TIME;

class boss:public Surfaces
{
    public:
        boss (char *);				//Takes filenames for left and right
       ~boss ();				// Frees the surfaces used in the 'right' and 'left' Image objects

        void apply_boss (int);			// display boss on screen
        void move ();				// To update position of boss
        void set_clips ();			// clips character image
        SDL_Rect getBossRect (int);		// returns the boss's position

        bool check_collision (SDL_Rect);	
        bool check_sides (SDL_Rect, SDL_Rect);	//Collision detection for main dying
        bool check_top (SDL_Rect, SDL_Rect);	//Collision detection for enemy dying
 
        vector < SDL_Rect > BOSS;		//Vector holds the position of the boss
        vector < SDL_Surface * > bossSheet;	//Vector holds the surface of the boss

    private:
        SDL_Rect bossclips[8];			// Image clips of the DuLachness Monster
       
        SDL_Surface *puff;			// The surface of the puff of smoke displayed after boss is killed
        SDL_Rect puff_coord;			// The coordinates of the puff
        int apply_puff;				// Indicates whether puff should be displayed
        int time;				// Used to display puff for certain period of time

        int frame;				// Current frame of Trojan
        int status;				// Direction the Trojan is facing
        int x, y;				// X and Y position screen       
        int xVel;				// x velocity of boss which oscillates based off of...
        float theta;				// ... angle theta


};


#endif
