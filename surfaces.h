/* Natalie Sanders, Katie Weiss, Kendra Bilardello
   surfaces.h
   Class for the management of loading, blitting, and initializing SDL surfaces
	 The main_character, Enemy, home_screen, and tile class all inherit from this class
*/

#ifndef SURFACES_H
#define SURFACES_H

#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "constants.h"
#include <string>

using namespace std;
extern SDL_Surface *screen;
extern SDL_Surface *background;
extern SDL_Rect camera;

class Surfaces
{
    public:
        Surfaces();
        bool init(); 				  // Initialize SDL surfaces
        SDL_Surface *load_files( string, float ); // Load image files to an SDL surface

	// Apply one SDL surface (with image) to another. This is mainly used in this
	// program to apply a surface onto the screen surface		
        void apply_surface( int, int, SDL_Surface* &, SDL_Surface* &, SDL_Rect* clip=NULL ); 

};


#endif
