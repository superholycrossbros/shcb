/* Natalie Sanders, Kendra Bilardello, Katie Weiss
   Enemy.h
   The interface for the class Enemy
   Contains a constructor which initializes two public Image objects right and left, which are really the two sides (right and left) of the enemy
   Also contains a function which interprets user data to display the enemy's left side when the left arrow is pressed and the right side when the right is pressed. */

#ifndef ENEMY_H
#define ENEMY_H

#include "constants.h"
#include "surfaces.h"
#include <vector>

using namespace std;

extern SDL_Surface *screen;
extern SDL_Surface *background;
extern SDL_Rect camera;
extern int TOTAL_ENEMIES;
extern int LIVES;
extern double POINTS;
extern int TIME;

class Enemy : public Surfaces
{
    public:
        Enemy(char *); //Takes filenames for left and right
	~Enemy(); // Frees the surfaces used in the 'right' and 'left' Image objects
		
	void apply_enemy(int);		// put enemies on screen
	void move();			// update enemies' positions
	void reset_positions();		// reset enemies to their original positions for restart of level
	void set_clips();  		// clips character image
	SDL_Rect getEnemyRect(int);	// returns coordinates of enemy i
						                         
	bool check_collision(SDL_Rect);		// Calls top collision (if true, enemy dies, points added, puff displayed) and side collision (if true character dies, lives decremented)
	bool check_sides(SDL_Rect, SDL_Rect); 	// Collision detection for character running into enemy sides
	bool check_top(SDL_Rect, SDL_Rect); 	// Collision detection for character running into enemy top

	vector <SDL_Rect> ENEMIES; 		// Vector holds the positions of the enemies
	vector <SDL_Surface *> enemySheet; 	// Vector holds the surfaces of the enemies
				
    private:
        SDL_Rect EnemyclipsRight[ 6 ]; 		// Image clips of the character walking right
	SDL_Rect EnemyclipsLeft[ 6 ];  		// Image clips of the character walking left
	SDL_Surface *puff;			// The surface of the puff which appears when enemy is killed
	SDL_Rect puff_coord;			// Coordinates of puff
	int apply_puff;				// Indicates whether puff should be applied
	int time_puff;				// Used to show puff for certain period of time

	int frame;   //Current frame of Trojan
	int status;  //Direction the Trojan is facing
	int x,y;     //X and Y position screen
	int xVel;    // x-velocity of Trojan
	float theta; // constantly increasing angle used to change xVel via sine to create oscillating motion
	
	
};


#endif
