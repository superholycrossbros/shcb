/* Natalie Sanders, Katie Weiss, Kendra Bilardello
   main.cpp
   Class for the movement of a character on the screen
*/



/*Please note that much of the implementation of this game is derived
  from the Lazyfoo SDL tutorials. This content includes, but is not 
  limited to: SDL rectangles, collision detection, movement, clips,
  sprites, maps, surface biltting.
*/


#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "SDL/SDL_ttf.h"
#include <string>
#include <iostream>
#include <vector>
#include "main_character.h"
#include "timer.h"
#include "surfaces.h"
#include "tile.h"
#include "transition_screens.h"
#include "Enemy.h"
#include "boss.h"
#include "shamrock.h"
#include <cmath>
#include "Sound.h"

// Global variables for the screen, it's background, and the camera
SDL_Surface *screen = NULL;
SDL_Surface *background = NULL;
SDL_Surface *tileSheet = NULL;
SDL_Surface *enemySheet = NULL;
bool LostGame = false;
int TOTAL_TILES = 0;
int TOTAL_ENEMIES = 0;
int TOTAL_SHAMROCKS = 0;
int LIVES = 5;
int TIME = 700;
int BOSS_HIT_COUNT = 5;
double POINTS = 0;
string CHARACTER_FILE = "Leprechaun_Clip.png";	// default leprechaun character


SDL_Rect camera = { 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT };


void
set_camera (main_character & MAIN)
{
  //Center the camera over the dot
  camera.x = (MAIN.coord.x + FOO_WIDTH / 2) - SCREEN_WIDTH / 2;
  camera.y = (MAIN.coord.y + FOO_HEIGHT / 2) - 3 * SCREEN_HEIGHT / 4;	///2;

  //Keep the camera in bounds.
  if (camera.x < 0)
    {
      camera.x = 0;
    }
  if (camera.y < 0)
    {
      camera.y = 0;
    }
  if (camera.x > LEVEL_WIDTH - camera.w)
    {
      camera.x = LEVEL_WIDTH - camera.w;
    }
  if (camera.y > LEVEL_HEIGHT - camera.h)
    {
      camera.y = LEVEL_HEIGHT - camera.h;
    }
}

void
clean_up (Sound music)
{
  // free music and sound in Sound object
  music.clean_up ();

  // Free background surface pointer; screen is freed in SDL_Quit and the character's surface pointer is freed in the deconstructor
  SDL_FreeSurface (background);
  SDL_FreeSurface (tileSheet);
  SDL_FreeSurface (screen);
  SDL_FreeSurface (enemySheet);

  //Quit SDL
  SDL_Quit ();
}





///////////////////// MAIN FUNCTION ////////////////////////////////////////

int
main (int argc, char *args[])
{  
  // play music throughout game
  Sound music (" ", "theme_song.wav");
  music.play ();

  bool quit = false; //Quits out of game if true




  while (quit == false)
    {

      int level = 0;
      int screen_character = 0;	// If 1, it will go to character screen


      SDL_Event event_screen;


      //Need this if statement so deconstructor is called once
      //start_surface and start_screen go out of scope
      if (level == 0)
	{
	  Surfaces start_surface;	//Instantiates a surface object for home


	  if (start_surface.init () == false)
	    {
	      return 1;
	    }

	  if (TTF_Init () == -1)
	    {
	      return false;
	    }

	  transition_screens start_screen;	        //Instantiates a transition_screens object
	  start_screen.display_main (start_surface, 0);	// Displays start instructions

	  if (SDL_Flip (screen) == -1)
	    {
	      return 1;
	    }



///////////////////////////HOME SCREEN////////////////////////////////

	  while (level == 0 && quit == false && screen_character == 0)	//While user hasn't quit or started
	    {
	      while (SDL_PollEvent (&(event_screen)))
		{
		  int x = 0;
		  int y = 0;

		  // if mouse is moving ...
		  if (event_screen.type == SDL_MOUSEMOTION)
		    {
		      //Get the mouse offsets 
		      x = event_screen.motion.x;
		      y = event_screen.motion.y;

		      //... and the mouse is over a button
		      if ((x > 550) && (x < 700) && (y >= 370) && (y < 410))		// START
			{
			  start_screen.display_main (start_surface, 1); // displays 'start' in blue
			}
		      else if ((x > 550) && (x < 680) && (y >= 410) && (y < 440))	// QUIT
			{
			  start_screen.display_main (start_surface, 2); // displays 'quit' in blue
			}
		      else if ((x > 500) && (x < 740) && (y >= 440) && (y < 470))	// CHARACTERS
			{
			  start_screen.display_main (start_surface, 3); // displays 'characters' in blue
			}
		      else
			{
			  start_screen.display_main (start_surface, 0); // all messages are displayed in white
			  if (SDL_Flip (screen) == -1)
			    {
			      return 1;
			    }
			}
		    }
		  else if (event_screen.type == SDL_MOUSEBUTTONDOWN)
		    {
		      if (event_screen.button.button == SDL_BUTTON_LEFT)
			{
			  //Get the mouse offsets 
			  x = event_screen.button.x;
			  y = event_screen.button.y;

			  //If the button clicks...
			  if ((x > 550) && (x < 700) && (y >= 370) && (y < 410))	// START
			    {
			      level = 1;
			      break;
			    }
			  else if ((x > 550) && (x < 680) && (y >= 410) && (y < 440))	// QUIT
			    {
			      quit = true;
			      break;
			    }
			  else if ((x > 500) && (x < 740) && (y >= 440) && (y < 480))	// CHARACTERS
			    {
			      screen_character = 1;
			      break;
			    }
			}
		    }
		  else if (event_screen.type == SDL_QUIT)
		    {
		      //Quit the program
		      quit = true;
		    }
		}
	    }
	}

      if (SDL_Flip (screen) == -1)
	{
	  return 1;
	}


/////////////////////DISPLAYING CHARACTER OPTIONS////////////////////////


//Need this if statement so character_surface and character_screen
//go out of scope once the character screen is no longer needed

      // If the user clicked the character button
      if (screen_character == 1 && quit == false)
	{

	  Surfaces character_surface;		// Surface object for character option screen

	  transition_screens character_screen;	// Transition_screens object for character option screen

	  if (character_surface.init () == false)
	    {
	      return 1;
	    }


	  //Put options for characters on screen
	  character_screen.display_character (character_surface, 0);

	  // Loops until game is started or the window is quitted, waiting for input
	  while (level == 0 && quit == false)
	    {
	      while (SDL_PollEvent (&(event_screen)))
		{
		  // left, right, top, and bottom of leprechaun on screen
		  int char1_L = SCREEN_WIDTH / 4 - FOO_WIDTH / 2 - 50;
		  int char1_R = char1_L + FOO_WIDTH + 200;
		  int char1_T = SCREEN_HEIGHT / 2 - FOO_HEIGHT / 2 - 50;
		  int char1_B = char1_T + FOO_HEIGHT + 100;

		  // left, right, top, and bottom of Prof. Emrich on screen
		  int char2_L = 3 * SCREEN_WIDTH / 4 - FOO_WIDTH / 2 - 50;
		  int char2_R = char2_L + FOO_WIDTH + 200;
		  int char2_T = SCREEN_HEIGHT / 2 - FOO_HEIGHT / 2 - 50;
		  int char2_B = char2_T + FOO_HEIGHT + 150;

		  int x = 0;
		  int y = 0;

		  if (event_screen.type == SDL_MOUSEMOTION)
		    {
		      //Get the mouse offsets 
		      x = event_screen.motion.x;
		      y = event_screen.motion.y;

		      // If the mouse is over the Prof. Emrich, displays 'Prof. Emrich' in blue
		      if ((x > char1_L) && (x < char1_R) && (y >= char1_T)
			  && (y < char1_B))
			{
			  character_screen.display_character
			    (character_surface, 1);
			}
		      // If the mouse is over the Prof. Emrich, displays 'Prof. Emrich' in blue 
		      else if ((x > char2_L) && (x < char2_R)
			       && (y >= char2_T) && (y < char2_B))
			{
			  character_screen.display_character
			    (character_surface, 2);
			}
		      // Otherwise both labels will remain white
		      else
			{
			  character_screen.display_character
			    (character_surface, 0);
			}
		    }
		  // If the left mouse button was clicked
		  else if (event_screen.type == SDL_MOUSEBUTTONDOWN)
		    {
		      if (event_screen.button.button == SDL_BUTTON_LEFT)
			{
			  //Get the mouse offsets 
			  x = event_screen.button.x;
			  y = event_screen.button.y;

			  //If the mouse is over the leprechaun button 
			  if ((x > char1_L) && (x < char1_R) && (y >= char1_T) && (y < char1_B))	// leprechaun
			    {
			      level++;
			      CHARACTER_FILE = "Leprechaun_Clip.png";
			    }
			  //If the mouse is over the Prof. Emrich button
			  else if ((x > char2_L) && (x < char2_R) && (y >= char2_T) && (y < char2_B))	// Prof. Emrich
			    {
			      level++;
			      CHARACTER_FILE = "Scotty_Clip.png";
			    }
			}
		    }
		  else if (event_screen.type == SDL_QUIT)
		    {
		      //Quit the program
		      quit = true;
		    }
		}
	    }
	}

////////////////////Game play actually begins here/////////////////////

      //Initialize important variables

      bool end_level = false;	// indicates whether player died or level is over
      int prev_lives = LIVES;	// used to check if the player has lost a life
      char *tile_map;		// name of map file for a level's bricks
      char *enemy_map;		// name of map file for a level's trojans 
      char *level_file;		// name of png file for the level background
      char *sham_map;		// name of map file for a level's shamrocks
      char *boss_map;		// name of map file for a level's boss (blank if not level 5)

      // Loads map files specific to each level
      while (level <= 5 && quit == false && LostGame == false)
	{

	  if (level == 1)
	    {
	      level_file = "level1.png";
	      tile_map = "level1.map";
	      enemy_map = "enemy1.map";
	      boss_map = "blank.map";
	      sham_map = "shamrock1.map";
	    }
	  else if (level == 2)
	    {
	      level_file = "level2.png";
	      tile_map = "level2.map";
	      enemy_map = "enemy2.map";
	      boss_map = "blank.map";
	      sham_map = "shamrock2.map";
	    }
	  else if (level == 3)
	    {
	      level_file = "level3.png";
	      tile_map = "level3.map";
	      enemy_map = "enemy3.map";
	      boss_map = "blank.map";
	      sham_map = "shamrock3.map";
	    }
	  else if (level == 4)
	    {
	      level_file = "level4.png";
	      tile_map = "level4.map";
	      enemy_map = "enemy4.map";
	      boss_map = "blank.map";
	      sham_map = "shamrock4.map";
	    }
	  else if (level == 5)
	    {
	      level_file = "level5.png";
	      tile_map = "level5.map";
	      enemy_map = "enemy5.map";
	      boss_map = "final_boss.map";
	      sham_map = "shamrock5.map";
	    }

	  // Displays next level screen
	  if (level <= 5 && quit == false)
	    {

	      Surfaces next_level_surface;	// Surface for next level
	      transition_screens next_level_screen;	// Object for next level
	      if (next_level_surface.init () == false)
		{
		  return 1;
		}

	      // Turns screen to black
	      if (SDL_Flip (screen) == -1)
		{
		  return 1;
		}

	      // Displays level number 
	      music.stop ();
	      next_level_screen.display_next_level (next_level_surface,
						    level);

	      if (SDL_Flip (screen) == -1)
		{
		  return 1;
		}

/////////////////// OBJECTS FOR GAME /////////////////////


	      // The main surface and transition object for each level's background
	      Surfaces level_surface;
	      transition_screens points_lives;
	      //Initialize the level surface
	      if (level_surface.init () == false)
		{
		  return 1;
		}


	      background = level_surface.load_files (level_file, 1);	// The main surface for each level's background
	      Timer fps;	// The frame rate regulator
	      main_character mainChar (CHARACTER_FILE);	// The main character
	      tile tiles (tile_map);	// Tile obstacles
	      Enemy enemies (enemy_map);	// Trojan enemies
	      boss bosses (boss_map);	// DuLacNess monster boss
	      Shamrock shamrocks (sham_map);

	      music.play ();

	      //While the user hasn't quit or finished level
	      while (end_level == 0 && quit == false)
		{
		  fps.start ();	// Start the frame timer
		  int time = fps.get_ticks ();	// retrieves current 

		  //While there are events to handle
		  while (SDL_PollEvent (&(mainChar.event)))
		    {
		      //Handle events for the character
		      mainChar.handle_events (level);

		      //If the user has Xed out the window
		      if (mainChar.event.type == SDL_QUIT)
			{
			  //Quit the program
			  quit = true;
			}
		    }

		  // Update character position
		  end_level = mainChar.move (tiles, end_level, enemies, bosses, shamrocks);	// Update character position
		  enemies.move ();	// Update enemy position to pace back and forth        
		  bosses.move ();	// Update DuLacNess Monster position

		  //Set the camera to the character
		  set_camera (mainChar);

		  //Keep background in display field
		  if (mainChar.coord.x < LEVEL_WIDTH)
		    {
		      level_surface.apply_surface (-mainChar.coord.x, 0,
						   background, screen);
		    }


		  // Display screen elements
		  shamrocks.apply_shamrock (mainChar.coord.x);	// Display shamrocks       
		  enemies.apply_enemy (mainChar.coord.x);	// Display enemies

		  if (level == 5)
		    {
		      bosses.apply_boss (mainChar.coord.x);	// Display boss if level 5
		    }

		  mainChar.show (tiles, enemies, bosses, shamrocks);	// Display character
		  tiles.apply_tileSurfaces (mainChar.coord.x);		// Display tiles
		  points_lives.display_points_lives (level_surface);	// Display score/lives


		  // Update the screen
		  if (SDL_Flip (screen) == -1)
		    {
		      return 1;
		    }

		  // Cap the frame rate
		  if (fps.get_ticks () < 1000 / FRAMES_PER_SECOND)
		    {
		      SDL_Delay ((1000 / FRAMES_PER_SECOND) -
				 fps.get_ticks ());
		    }

		  // Update time
		  TIME -= fps.get_ticks () / 100;

		  // If you lost a life
		  if (prev_lives > LIVES)
		    {
		      // update previous lives and set variable to end level
		      prev_lives = LIVES;
		      end_level = 1;

		      if (LIVES != 0)
			{
			  level = level - 1;	// So that the when level is incremented it will stay on same level
			}
		    }

		  // If you lost all lives
		  if (LIVES <= 0)
		    {	      
		      end_level = 1;
		      LostGame = true;
		    }

		}

	      enemies.reset_positions ();	// reinitialize enemies start positions
	      end_level = false;		// reinitialize end_level
	      level++;
	      TIME = 700;			// restart timer

	    }

	}

///////////////////////////////GAME OVER/////////////////////////////////

      int restart = 0;
      Surfaces end_surface;		// Surface for end surface
      transition_screens end_screen;	// Object for end screen

      if (end_surface.init () == false)
	{
	  return 1;
	}

      //////// WON /////////

      // If you won the game
      if (level > 5 && quit == false && LostGame == false)
	{
	  music.stop(); // music is stopped to let 'won' sound play

	  // Displays congratulatory message and instruction to get back to home screen
	  end_screen.end_level (end_surface, 0);
	  
	  // Waits for user input
	  while (quit == false && restart == 0)
	    {
	      while (SDL_PollEvent (&(event_screen)))
		{
		  int x = 0;
		  int y = 0;

		  // If mouse moves
		  if (event_screen.type == SDL_MOUSEMOTION)
		    {
		      //Get the mouse offsets 
		      x = event_screen.motion.x;
		      y = event_screen.motion.y;

		      SDL_Rect button = end_screen.getButton ();	// gets position of button

		      //If the mouse is over the home button 
		      if ((x >= button.x) && (x <= (button.x + button.w))
			  && (y >= button.y) && (y <= button.y + button.h))
			{
			  end_screen.end_level (end_surface, 1);	// button appears pressed in blue
			}
		      else
			{
			  end_screen.end_level (end_surface, 0);	// button stays white
			}
		    }
		  // If the left mouse button is clicked
		  else if (event_screen.type == SDL_MOUSEBUTTONDOWN)
		    {
		      if (event_screen.button.button == SDL_BUTTON_LEFT)
			{
			  //Get the mouse offsets 
			  x = event_screen.button.x;
			  y = event_screen.button.y;

			  SDL_Rect button = end_screen.getButton ();	// gets position of button

			  //If the home button is clicked, variables are reinitialized to display home screen
			  if ((x >= button.x) && (x <= (button.x + button.w))
			      && (y >= button.y)
			      && (y <= button.y + button.h))
			    {

			      restart = 1;

			      LIVES = 5;
			      BOSS_HIT_COUNT = 5;
			      POINTS = 0;
			      CHARACTER_FILE = "Leprechaun_Clip.png";
			      level = 0;
			      screen_character = 0;

			      music.play();
			      break;
			    }
			}
		    }
		  else if (event_screen.type == SDL_QUIT)
		    {
		      //Quit the program
		      quit = true;
		    }
		}
	    }
	}


      ////////////////// LOST /////////////////////

      // If you lost the game (all lives were lost)
      if (LostGame == true && quit == false)
	{
	  music.stop();
	  LostGame = false;	// reinitialize variable for next game

	  // Displays losing message and instruction to get back to home screen
	  end_screen.lost (end_surface, 0);

	  // Wait for user input
	  while (quit == false && restart == 0)
	    {
	      while (SDL_PollEvent (&(event_screen)))
		{
		  int x = 0;
		  int y = 0;
		  // If the mouse moved
		  if (event_screen.type == SDL_MOUSEMOTION)
		    {
		      //Get the mouse offsets 
		      x = event_screen.motion.x;
		      y = event_screen.motion.y;

		      SDL_Rect button = end_screen.getButton ();

		      //If the mouse is over the button 
		      if ((x >= button.x) && (x <= (button.x + button.w))
			  && (y >= button.y) && (y <= button.y + button.h))
			{
			  end_screen.lost (end_surface, 1);	// button appears pressed in blue
			}
		      else
			{
			  end_screen.lost (end_surface, 0);	// button remains white
			  if (SDL_Flip (screen) == -1)
			    {
			      return 1;
			    }
			}
		    }
		  // If left mouse button is clicked
		  else if (event_screen.type == SDL_MOUSEBUTTONDOWN)
		    {
		      if (event_screen.button.button == SDL_BUTTON_LEFT)
			{
			  //Get the mouse offsets 
			  x = event_screen.button.x;
			  y = event_screen.button.y;

			  SDL_Rect button = end_screen.getButton ();	// get button's coordinates

			  //If the mouse is over the button, variables are initialized to go back to home screen
			  if ((x >= button.x) && (x <= (button.x + button.w))
			      && (y >= button.y)
			      && (y <= button.y + button.h))
			    {
			      restart = 1;

			      LIVES = 5;
			      BOSS_HIT_COUNT = 5;
			      POINTS = 0;
			      CHARACTER_FILE = "Leprechaun_Clip.png";
			      level = 0;
			      screen_character = 0;

			      music.play();

			    }
			}
		    }
		  else if (event_screen.type == SDL_QUIT)
		    {
		      //Quit the program
		      quit = true;
		    }
		}
	    }
	}
    }

  clean_up (music);
  return 0;
}
