/* Natalie Sanders, Katie Weiss, Kendra Bilardello
   tile.h
   Class for putting blocks on screen
*/

#include "tile.h"
#include "surfaces.h"
#include <fstream>
#include <sstream>

using namespace std;

tile::tile()
{
    ifstream infile;
    infile.open ("level1.map");
    int value;	

    if (infile) // Read until end of file 
    {
        string line;
        while (getline(infile, line)) //Reads in full line
        {
            SDL_Rect temp;
            temp.x = line[0];
            temp.y = line[1];
            temp.w = TILE_WIDTH;
            temp.h = TILE_HEIGHT;

            TILES.push_back(temp); // Puts line into vector of vectors
        }
    }

    for(int i = 0; i < TOTAL_TILES; i++)
    {
        SDL_Surface *temp = NULL;
        temp = load_files("tiles.png", 1.0);
        if(temp == NULL)
        {
            tileSheet.push_back(temp);
        }
        
        SDL_FreeSurface(temp);
    }


    set_clip();
}

tile::~tile()
{
    // frees all tiles
    for(int i = 0; i < TOTAL_TILES; i++)
    {
        SDL_FreeSurface(tileSheet[i]);
    }
}

void tile::apply_tileSurfaces(int x)
{
    // applies all tiles to the screen
    for (int i=0; i<TOTAL_TILES; i++)
    {   
        apply_surface(TILES[i].x - camera.x-x, TILES[i].y - camera.y, tileSheet[i], screen, &clip);
    }
}


void tile::show()
{
    int i=1;
    for (int i=0; i<TOTAL_TILES; i++){
       // apply_surface( wall[i].x - camera.x, wall[i].y - camera.y, tileSheet, screen, &clips[ 0 ] );
			}

		cout << "Box x position: "<< TILES[i].x-camera.x<< endl;
		cout << "Box y postion: " << TILES[i].y-camera.y <<endl;



   //}
}

SDL_Rect tile::getTileRect(int i)
{
    return TILES[i];
}

void tile::set_clip()
{
    clip.x = 0;
    clip.y = 0;
    clip.w = TILE_WIDTH;
    clip.h = TILE_HEIGHT;
}


