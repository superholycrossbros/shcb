/* Natalie Sanders, Katie Weiss, Kendra Bilardello
   main_character.h
   Class for the movement of a character on the screen
*/

#ifndef MAIN_CHARACTER_H
#define MAIN_CHARACTER_H

#include "constants.h"
#include "surfaces.h"
#include "tile.h"
#include "Enemy.h"
#include "boss.h"
#include "shamrock.h"
#include "Sound.h"

extern SDL_Surface *screen;
extern SDL_Surface *background;
extern SDL_Rect camera;
extern int TOTAL_TILES;		// used in collision detection
extern int TOTAL_ENEMIES;	// used in collision detection
extern double POINTS;
extern int BOSS_HIT_COUNT;


//The stick figure
class main_character : public Surfaces
{
   public:
    main_character(string); 
    ~main_character();
    void set_clips();		// clips character image

    void handle_events(int);	// Deals with key presses
    int move(tile &,bool, Enemy &, boss &, Shamrock &);  // Moves the character
    void show(tile &, Enemy &, boss &, Shamrock &);      // displays character


    SDL_Rect coord;		// x/y position of character and w/h
    SDL_Rect vel;		// x/y velocity of character

   // SDL_Rect wall; 		// the obstacle
    int wall_height; 		// current altitude of character above the main 'ground'
    
    SDL_Event event; 		// event variable used to keep track of key presses

   private:
    int frame;                	// Current frame of the character
    int status;               	// Current animation status

    SDL_Rect clipsRight[ 7 ]; 	// Image clips of the character walking right
    SDL_Rect clipsLeft[ 7 ];  	// Image clips of the character walking left
    SDL_Surface *mainC;       	// The surface for the character image  
    int currentBlock;         	// The block the character is standing on (-1 if not any)
    Sound jump;		      	// sound played when the character jumps
    int boss_hit;	      	// indicates the character just hit a boss
    int key_up;			// indicates whether to wait for a key to be pressed before recognizing a released key; important for character velocity after hitting boss


};

#endif
