/* Natalie Sanders, Image.cpp
   The implementation of the class Image
   Defines functions necessary for loading an image file to the SDL_Surface pointer m_image and applying this surface to the screen in a specific location and scale
   Also, defines functions to get the height and width of the image and to free the SDL_Surface pointer m_image */

#include "Image.h"

Image::Image(string file, float scale)
{
    m_filename = file;
    m_scale = scale;
    m_image = NULL;
}

bool Image::loadImage()
{
    SDL_Surface* loadedImage = NULL; // Surface to which the image is originally loaded

    SDL_Surface* optimizedImage = NULL; // Surface to which the optimized image is stored

    loadedImage = IMG_Load( m_filename.c_str() );

    // If the image loaded successfully
    if( loadedImage != NULL )
    {
        optimizedImage = SDL_DisplayFormatAlpha( loadedImage ); // the image is optimized
        SDL_FreeSurface( loadedImage ); // the old surface is freed
    }

    m_image  = rotozoomSurface( optimizedImage, 0., m_scale, 1); // scales the image to a specified fraction
    m_width  = m_image->w; // initializes m_width to the width of the image
    m_height = m_image->h; // initializes m_height to the width of the image

    return true;
}

void Image::applySurface( int x, int y, SDL_Surface* destination, SDL_Rect* clip)
{
    SDL_Rect offset; // the (x, y) coordinates used to place the image

    //Get x and y coordinates of the offset
    offset.x = x;
    offset.y = y;

    //Blitzes the image to the screen
    SDL_BlitSurface( m_image, clip, destination, &offset );
}

int Image::getWidth()
{
    return m_width;
}

int Image::getHeight()
{
    return m_height;
}

void Image::freeImage()
{
    SDL_FreeSurface( m_image );
}
