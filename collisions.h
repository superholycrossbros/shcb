/* Kendra Bilardello, Natalie Sanders, and Katie Weiss
  Super Holy Cross Bros. copyright 2014*/


#ifdef COLLISIONS_H
#define COLLISIONS_H

#include "tile.h"
#include "Enemy.h"
#include <vector>


class collisions {

   public:
	vector< T > collObjects; 		// vector to hold all objects that can be collided with

	virtual bool check_collision();		// virtual function to check for main character collisions

	collisions(tile,Enemy);			//Constructor to make the objects

	

