/* Natalie Sanders, Image.h
   The interface for the class Image
   Contains functions necessary for loading an image file to the SDL_Surface pointer m_image and applying this surface to the screen in a specific location and scale
   Also, contains functions to get the height and width of the image and to free the SDL_Surface pointer m_image
*/

#ifndef IMAGE_H
#define IMAGE_H

extern const int SCREEN_HEIGHT;
extern const int SCREEN_WIDTH;
extern const int SCREEN_BPP;

#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "SDL/SDL_rotozoom.h"
#include <string>
#include <iostream>

using namespace std;

class Image
{
    public:
        Image(string, float);                                        // Constructor takes the name of the image file to load and a float to scale the resulting surface m_image
        bool loadImage();                                            // Loads an image file, optimizing it, to the m_image surface; it is then scaled according to the float m_scale and initializes m_width and m_height
        int  getWidth();                                             // Returns m_width
        int  getHeight();                                            // Returns m_height
        void freeImage();                                            // Frees m_image
        void applySurface(int, int, SDL_Surface*, SDL_Rect* = NULL); // Applies the surface m_image to the screen in a specified location

    private:
        SDL_Surface *m_image;  // the surface for the loaded image
        string m_filename;     // the image's filename
        float m_scale;         // a decimal number used to scale the original image (m_scale*100) percent
        int m_width, m_height; // the width and height of the image
};

#endif
