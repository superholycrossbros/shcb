/* Natalie Sanders, Kendra Bilardello, Katie Weiss
   shamrock.h
   Implementation of shamrock class which displays shamrocks and detects character collisions with shamrocks
*/

#include "shamrock.h"
#include "surfaces.h"

Shamrock::Shamrock (char *filename)
{
  FILE *f = fopen (filename, "r");	// Opens the input file and creates a file pointer
  if (!f)			// Prints error message if the file could not be opened
    {
      cout << "Could not open file" << endl;
    }
  else
    {

      char line[100];
      int j = 0;
      while (fgets (line, sizeof (line), f)) // Takes one line from the file at a time
	{
	  //Creates temporary variable to hold the position of the shamrock (of that line)
	  SDL_Rect temp;
	  temp.x = atoi (line);
	  temp.y = atoi (&line[5]);
	  temp.w = FOO_WIDTH;
	  temp.h = FOO_HEIGHT;
	  SHAMROCKS.push_back (temp); // Puts coordinates into vector of SDL_Rect
	}
      fclose (f);
    }

  TOTAL_SHAMROCKS = SHAMROCKS.size ();

  // Pushes shamrock surface pointers into the shamrockSheet vector
  for (int i = 0; i < TOTAL_SHAMROCKS; i++)
    {
      SDL_Surface *temp = NULL;
      temp = load_files ("shamrock.png", 0.6);
      if (temp != NULL)
	{
	  shamrockSheet.push_back (temp);
	}

    }

  // Loads image of +10 pts star to display when shamrock is collected
  puff = load_files ("star.png", 1);
  collected = 0;

  // Loads image of +1 life up star to display when 7 shamrocks are collected
  up   = load_files ("one_up.png", 1);
}

Shamrock::~Shamrock ()
{
  for (int i = 0; i < shamrockSheet.size (); i++)
    {
      SDL_FreeSurface (shamrockSheet[i]);
    }

  SDL_FreeSurface (puff);
  SDL_FreeSurface (up);

  collected = 0;

  apply_puff = 0;
  apply_1up = 0;

  SDL_Flip (screen);
}

void
Shamrock::check_collision (SDL_Rect charCoord, vector < SDL_Rect > blocks)
{
  int topCh = charCoord.y - camera.y + 60;		 // top of character
  int botCh = charCoord.y + FOO_HEIGHT - camera.y - 10;	 // bottom of character
  int rightCh = charCoord.x + FOO_WIDTH - camera.x - 10; // right of character
  int leftCh = charCoord.x - camera.x + 10;		 // left of character

  // Loops through all shamrocks to check for collision with each one
  for (int i = 0; i < TOTAL_SHAMROCKS; i++)
    {
      SDL_Rect shamCoord = SHAMROCKS[i]; // position of current shamrock

      int topSh = shamCoord.y - camera.y;				// top of shamrock
      int botSh = shamCoord.y + shamCoord.h - camera.y;			// bottom of shamrock
      int rightSh = shamCoord.x + shamCoord.w - camera.x - charCoord.x;	// right of shamrock
      int leftSh = shamCoord.x - camera.x - charCoord.x;		// left of shamrock

      float midSh_x = (rightSh - leftSh) / 2;
      float midSh_y = (botSh - topSh) / 2;

      int sham_valid = 1;

      for (int j = 0; j < blocks.size (); j++)
	{

	  SDL_Rect Bcoord = blocks[j];
	  int topT = Bcoord.y - camera.y;	// top of character
	  int botT = Bcoord.y + FOO_HEIGHT - camera.y;	// bottom of character
	  int rightT = Bcoord.x + FOO_WIDTH - camera.x;	// right of character
	  int leftT = Bcoord.x - camera.x;	// left of character
	}

          // If the character has run into a shamrock
	  if ((rightCh >= leftSh && rightCh <= leftSh + 15 && botCh >= topSh && topCh <= botSh) ||	// Right of character intersects with shamrock
	      (leftCh <= rightSh && leftCh >= rightSh - 15 && botCh >= topSh && topCh <= botSh) ||	// Left of character intersects with shamrock
	      (botCh >= topSh && botCh <= topSh + 15 && leftCh <= rightSh && rightCh >= leftSh) ||	// Bottom of character intersects with shamrock
	      (topCh <= botSh && topCh >= botSh - 15 && leftCh <= rightSh && rightCh >= leftSh))	// Top of character intersects with shamrock
	    {

	      // Set up variables to apply +10 star
	      apply_puff = 1;
	      puff_coord.x = SHAMROCKS[i].x - camera.x - charCoord.x + 20;
	      puff_coord.y = SHAMROCKS[i].y - camera.y - 70;

	      // 10 points for every shamrock collected
	      POINTS += 10;



	      // if 7 shamrocks are collected, 1 life is given
	      collected++;

	      if (collected%7 == 0)
		{
		  LIVES++;
		  collected = 0; // restart count

		  // Set up variable to apply +1 life up star
		  apply_1up = 1;
		  up_coord.x = SHAMROCKS[i].x - camera.x - charCoord.x + 70;
		  up_coord.y = SHAMROCKS[i].y - camera.y - 70;
		}



	      //Remove shamrock from SDL_Surface vector and SDL_Rect vector; update # of shamrocks
	      SDL_FreeSurface (shamrockSheet[i]);
	      shamrockSheet.erase (shamrockSheet.begin () + i);
	      SHAMROCKS.erase (SHAMROCKS.begin () + i);
	      TOTAL_SHAMROCKS--;

	      // used to show puff/1up for a certain length of time
	      time_puff = TIME;
              time_1up  = TIME;
	    }
    }
}

void
Shamrock::apply_shamrock (int x)
{
  //If an shamrock was caught, apply_puff = 1 and the +10 star is applied
  if (apply_puff == 1)
    {
      apply_surface (puff_coord.x, puff_coord.y, puff, screen);
    }

  //Sets length of time to display the +10 star
  if (TIME < time_puff - 5)	
    {
      apply_puff = 0;

    }

  // If 7 shamrocks were caught, apply_1up = 1 and the +1 up star is applied
  if (apply_1up == 1)// && puff_coord.x != -1 && puff_coord.y != -1)
    {
      apply_surface (up_coord.x, up_coord.y, up, screen);
    }

  //Sets length of time to display the +1 up star
  if (TIME < time_1up - 5)	
    {
      apply_1up = 0;
    }

  // Applies all shamrocks to the screen
  for (int i = 0; i < shamrockSheet.size (); i++)
    {
      apply_surface (SHAMROCKS[i].x - camera.x - x, SHAMROCKS[i].y - camera.y, shamrockSheet[i], screen);
    }


}
