/* Kendra Bilardello, Natalie Sanders, Katie Weiss
   Sound.cpp
   Class for playing sounds/music
*/


#include "Sound.h"

Sound::Sound(string sound_file, string music_file)
{
    Mix_OpenAudio( 22050, MIX_DEFAULT_FORMAT, 2, 4096 ); // opens audio and initializes

    // if a sound_file was provided, it is loaded
    if(sound_file != " ")
    {
        sound = Mix_LoadWAV(sound_file.c_str());
        Mix_VolumeChunk(sound, 128);
    }
    else
    {
        sound = NULL;
    }

    // if a music file was provided, it is loaded
    if(music_file != " ")
    {
        music = Mix_LoadMUS(music_file.c_str());
	Mix_VolumeMusic(25);
    }
    else
    {
        music = NULL;
    }
}

void Sound::clean_up()
{
    // frees music and sound
    if(sound)
    {
        Mix_FreeChunk(sound);
    }

    if(music)
    {
        Mix_HaltMusic();
        Mix_FreeMusic(music);
    }

    Mix_CloseAudio();
}

void Sound::play()
{
    // plays sound or music
    if(sound)
    {
        Mix_PlayChannel( -1, sound, 0 );
    }
    else if(music)
    {
        Mix_PlayMusic(music, -1);
    }
}

void Sound::stop()
{
    // stops music
    if(music)
    {
        Mix_HaltMusic();
    }
}
