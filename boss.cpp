/* Natalie Sanders, Kendra Bilardello, Katie Weiss
   boss.h
   The implementation for the class boss is pressed. */

#include "boss.h"
#include "surfaces.h"
#include <math.h>

using namespace std;

//Define constructor that will read in the boss file and put the values into a vector of SDL_Rects and a vector of SDL_Surfaces
boss::boss (char *file_name)
{
  FILE *f = fopen (file_name, "r");	// Opens the input file and creates a file pointer
  if (!f)			// Prints error message if the file could not be opened
    {
      cout << "Could not open file" << endl;
    }
  else
    {

      char line[100];
      int j = 0;
      while (fgets (line, sizeof (line), f))	// Takes one line from the file at a time
	{
	  //Creates temporary variable to hold the data
	  SDL_Rect temp;
	  temp.x = atoi (line);
	  temp.y = atoi (&line[5]);
	  temp.w = 278.5;
	  temp.h = 294;
	  BOSS.push_back (temp);	// Puts line into vector of vectors
	}
      fclose (f);
    }

  SDL_Surface *temp = NULL;
  temp = load_files ("Monster_Clip.png", 1.25);
  if (temp != NULL)
    {
      bossSheet.push_back (temp);	//Initializing sheet to display trojan enemies
    }



  puff = load_files ("puff_blank.png", 3);
  apply_puff = 0;
  set_clips ();
  frame = 0;
  status = 0;


}

//Loops through the vector of SDL_Surfaces and frees them
boss::~boss ()
{
  for (int i = 0; i < bossSheet.size (); i++)
    {
      SDL_FreeSurface (bossSheet[i]);
    }
  SDL_FreeSurface (puff);
}


void
boss::move ()
{
  int prev_xVel = xVel;
  xVel = 70 * sin (theta);
  theta += 0.1;			//Increment position of boss

  frame++;
}

void
boss::apply_boss (int x)
{
// Loop the animation (go though the stages of walking)
  if (frame >= 6)
    {
      frame = 0;
    }

      apply_surface (BOSS[0].x + xVel - camera.x - x, BOSS[0].y - camera.y,
		     bossSheet[0], screen, &bossclips[frame]);


  //If the boss was erased, apply_puff=1
  if (apply_puff == 1)
    {
      apply_surface (puff_coord.x, puff_coord.y, puff, screen);
    }

  if (TIME < time + 20)
    {				//Sets length of time to display the puff
      apply_puff = 0;
    }




}

//Returns the boss i
SDL_Rect
boss::getBossRect (int i)
{
  return BOSS[i];
}


//Clips the boss character image
void
boss::set_clips ()
{
  float w = 256*1.25;
  float h = 234*1.25;

  // boss walking

  bossclips[0].x = 0;
  bossclips[0].y = 3;
  bossclips[0].w = w;
  bossclips[0].h = h-3;

  bossclips[1].x = w;
  bossclips[1].y = 9;
  bossclips[1].w = w-2;
  bossclips[1].h = h-9;

  bossclips[2].x = w * 2;
  bossclips[2].y = 19;
  bossclips[2].w = w-2;
  bossclips[2].h = h-19;

  bossclips[3].x = w * 3;
  bossclips[3].y = 26;
  bossclips[3].w = w;
  bossclips[3].h = h-26;

  bossclips[4].x = 0;
  bossclips[4].y = h+25;
  bossclips[4].w = w-2;
  bossclips[4].h = h-25;

  bossclips[5].x = w;
  bossclips[5].y = h+20;
  bossclips[5].w = w-2;
  bossclips[5].h = h-20;

  bossclips[6].x = w * 2;
  bossclips[6].y = h+10;
  bossclips[6].w = w-2;
  bossclips[6].h = h-10;

  bossclips[7].x = w * 3;
  bossclips[7].y = h+4;
  bossclips[7].w = w;
  bossclips[7].h = h-4;

}

bool
boss::check_collision (SDL_Rect coord)
{

  // If character jumps on top of boss
  if (check_top (getBossRect (0), coord))
    {

      POINTS += 250;
      BOSS_HIT_COUNT--; // "weaken" boss

      // If user kills boss (jumps on top of him 5 times)
      if (BOSS_HIT_COUNT == 0)
	{
	  apply_puff = 1;
	  puff_coord.x = BOSS[0].x - camera.x - coord.x + xVel;
	  puff_coord.y = BOSS[0].y - camera.y;
	 
	  // remove surface and coordinates from boss vectors
	  bossSheet.erase (bossSheet.begin () + 0);
	  BOSS.erase (BOSS.begin () + 0);

	  // used to apply puff for certain period of time
	  time = TIME;
	  POINTS += 1000;
	}

	return true; // returns false to indicate that the boss was jumped on
    }
  
  else if (check_sides (getBossRect (0), coord))
    {
      BOSS_HIT_COUNT = 5;
      if(LIVES != 0) LIVES--;
    }

    return false; // returns false to indicate that the boss was not jumped on
}




//A special collision detection that helps regulate the lives of the main_character. If the main_character runs into the right or left of the Boss, he will die and this funciton will return true.
bool
boss::check_sides (SDL_Rect B, SDL_Rect coord)
{
  int leftA, leftB;
  int rightA, rightB;
  int topA, topB;
  int bottomA, bottomB;

  //Calculate the sides of Rect A, the character
  leftA = coord.x - camera.x + 10;
  rightA = coord.x + FOO_WIDTH - camera.x - 25;
  topA = coord.y - camera.y + 10;
  bottomA = coord.y + FOO_HEIGHT - camera.y;

  //Calculate the sides of Rect B, the boss
  leftB = B.x + xVel - camera.x - coord.x;
  rightB = B.x + xVel + B.w - camera.x - coord.x;
  topB = B.y - camera.y + 40;
  bottomB = B.y + B.h - camera.y;


  if (rightA >= leftB + 10 && rightA <= leftB + 150 && bottomA >= topB
      && topA <= bottomB)
    {

      return true;		//Right of character overlaps with Trojan

    }
  else if (leftA <= rightB - 10 && leftA >= rightB - 150 && bottomA >= topB
	   && topA <= bottomB)
    {
      return true;		//Left of character overlaps with Trojan
    }


  return false;			//No side of the character overlapped
}


//Collision detection for the top of the boss to determine if the main_character killed the boss
bool
boss::check_top (SDL_Rect B, SDL_Rect coord)
{
  int leftA, leftB;
  int rightA, rightB;
  int topA, topB;
  int bottomA, bottomB;

  //Calculate the sides of Rect A, the character
  leftA = coord.x - camera.x + 10;
  rightA = coord.x + FOO_WIDTH - camera.x - 25;
  topA = coord.y - camera.y + 10;
  bottomA = coord.y + FOO_HEIGHT - camera.y;

  //Calculate the sides of Rect B, the boss
  leftB = B.x + xVel - camera.x - coord.x;
  rightB = B.x + xVel + B.w - camera.x - coord.x;
  topB = B.y - camera.y;
  bottomB = B.y + B.h - camera.y;

  // Top of boss overlaps with bottom of character
  if (bottomA <= topB + 100 && bottomA >= topB && leftA <= rightB
      && rightA >= leftB)
    {

      return true;		//boss was killed and should be removed from screen
    }


  return false;			//The main_character did not land on top of boss

}
