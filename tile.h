/* Natalie Sanders, Katie Weiss, Kendra Bilardello
   tile.h
   Class for putting the tiles (blocks) onto the screen
*/

#ifndef TILE_H
#define TILE_H

#include "constants.h"
#include "surfaces.h"
#include <vector>

using namespace std;

extern SDL_Surface *screen;
extern SDL_Surface *background;
extern SDL_Rect camera;
extern int TOTAL_TILES;
extern int TIME;

class tile : public Surfaces
{
    public:
        tile(char*); 			// Constructor reads in a file of tile positions
        ~tile(); 			// Frees all tiles
        void apply_tileSurfaces(int); 	// Applies all tile surfaces to the screen
        void set_clips(); 		// Set the clips for the tile
        bool check_topbot_collision(SDL_Rect, SDL_Rect, int, int *, int*); // checks if character hit top or bottom of block
        bool check_side_collision(SDL_Rect, SDL_Rect, int, int *, int*);   // checks if character hit a side of the block
        vector < SDL_Rect > getTILES(); 				   // Returns the dimensions of a tile in the TILES vector

    private:
        vector< SDL_Rect> TILES; 		// Vector of SDl_Rect to hold the position of the tiles 
        SDL_Rect clip; 				// Holds the position of the tiles
        vector < SDL_Surface * > tileSheet; 	// Vector of SDL_Surfaces to put tiles on screen
	vector < int > breakable; 		// 1/0 tells whether the block is able to be smashed

	SDL_Surface *puff; 			// The surface of the breaking block puff
	SDL_Rect puff_coord;			// Position of the breaking block puff
	int apply_puff;				// Indicates whether or not to apply puff surface
        int time_tile;				// Used to display the puff for a limited time period

};


#endif
