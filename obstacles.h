/* Natalie Sanders, Katie Weiss, Kendra Bilardello
   obstacles.h
   Class for the obstacles/level on the screen
*/

#ifndef OBSTACLES_H
#define OBSTACLES_H

#include "constants.h"
#include "surfaces.h"

class obstacles : public Surfaces
{
	public:
		obstacles();
		obstacles(int, int, std::string);
		~obstacles();
		SDL_Rect *collitionZones;
		SDL_Rect *block;
		void setCollitionCoords(int, SDL_Rect);
		int x;
		int y;
		int w;
		int h;
		void setX(int);
		void setY(int);
		void setW(int);
		void setH(int);
		SDL_Rect getCollitionAreas(int);
		static int numberOfObjects;
		SDL_Surface* sprite;


};





#endif
