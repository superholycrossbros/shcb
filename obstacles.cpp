/* Natalie Sanders, Katie Weiss, Kendra Bilardello
   obstacles.h
   Functions for class for the obstacles/level on the screen
*/

#include "obstacles.h"
#include "surfaces.h"



obstacles::obstacles()
{
	x=0;
	y=0;
	numberOfObjects++;


}

obstacles::obstacles(int posX, int posY, std::string file)
{
	x=posX;
	y=posY;
	sprite = Surfaces::load_files(file);
	numberOfObjects++;


}

obstacles::~obstacles()
{
	numberOfObjects--;

}

void obstacles::setCollitionCoords(int a, SDL_Rect offsets)
{
	collitionZones[a].x = offsets.x;
	collitionZones[a].y = offsets.y;
	collitionZones[a].w = offsets.w;
	collitionZones[a].h = offsets.h;

}


SDL_Rect obstacles::getCollitionAreas(int a)
{
	SDL_Rect offsets;
	offsets.x = x +(collitionZones+a)->x;
	offsets.y = y +(collitionZones+a)->y;
	offsets.w = (collitionZones+a)->w;
	offsets.h = (collitionZones+a)->h;

	return offsets;

}

void obstacles::setX(int posX)
{
	x=posX;


}
void obstacles::setY(int posY)
{
	y=posY;


}

void obstacles::setW(int posW)
{
	w=posW;


}

void obstacles::setH(int posH)
{
	h=posH;


}






