/* Natalie Sanders, Katie Weiss, Kendra Bilardello
   home_screen.h
   Functions for class for the images on the home/main_screen
*/

#include "home_screen.h"
#include <sstream>
#include <string.h>

using namespace std;

//Constructor will initialized all values to NULL and load the start screen graphic initially
home_screen::home_screen()    
{
    //Initialize surfaces
    start_screen_graphic = load_files("start_screen.png", 1);
    start_message        = NULL;
    quit_message         = NULL;
    character_message    = NULL;

    leprechaun_message   = NULL;
    Scotty_message       = NULL;
    char_option1         = NULL;
    char_option2         = NULL;

    level1_message       = NULL;
    level2_message       = NULL;
    level3_message       = NULL;
    level4_message       = NULL;
    level5_message       = NULL;

		end_message 				 = NULL;
		lost_message				 = NULL;
		reset_message				 = NULL;

		score_message 			 = NULL;
		lives_message				 = NULL;

		score_value					 = NULL;
		lives_value					 = NULL;
    
    font                 = NULL;
}

//Deconstructor will be called when object goes out of scope. It will free all the surfaces at this time. 
home_screen::~home_screen()
{
    SDL_FreeSurface(start_screen_graphic);
    SDL_FreeSurface(start_message);
    SDL_FreeSurface(quit_message);
    SDL_FreeSurface(character_message);

    SDL_FreeSurface(leprechaun_message);
    SDL_FreeSurface(Scotty_message);
    SDL_FreeSurface(char_option1);
    SDL_FreeSurface(char_option2);

    SDL_FreeSurface(level1_message);
    SDL_FreeSurface(level2_message);
    SDL_FreeSurface(level3_message);
    SDL_FreeSurface(level4_message);
    SDL_FreeSurface(level5_message);

    SDL_FreeSurface(end_message);
		SDL_FreeSurface(lost_message);
    SDL_FreeSurface(reset_message);

		SDL_FreeSurface(score_value);
		SDL_FreeSurface(score_message);
		SDL_FreeSurface(lives_message);
		SDL_FreeSurface(lives_value);
    
    TTF_CloseFont(font);
}

//Sets all the messages in one function. Each message will be put on the screen using a "mario" font
void home_screen::set_messages(int button_hover)
{
    SDL_Color black = {0x00, 0x00, 0x00}; //Color is black
    SDL_Color white = {0xFF, 0xFF, 0xFF}; //Color is white

    font = TTF_OpenFont("Super Mario Bros_.ttf", 18);// Small font size

		// If the mouse is over start or quit or character, that option
		//will be turned to white text
    if(button_hover == 1)
    {
        start_message= TTF_RenderText_Solid(font, "START", white);
        quit_message = TTF_RenderText_Solid(font, "QUIT", black);
        character_message = TTF_RenderText_Solid(font, "CHARACTER", black);
    }
    else if (button_hover == 2)
    {
        start_message= TTF_RenderText_Solid(font, "START", black);
        quit_message = TTF_RenderText_Solid(font, "QUIT", white);
        character_message = TTF_RenderText_Solid(font, "CHARACTER", black);
    }
    else if (button_hover == 3)
    {
        start_message= TTF_RenderText_Solid(font, "START", black);
        quit_message = TTF_RenderText_Solid(font, "QUIT", black);
        character_message = TTF_RenderText_Solid(font, "CHARACTER", white);
    }
    else if (button_hover == 0) 
    {
        start_message= TTF_RenderText_Solid(font, "START", black);
        quit_message = TTF_RenderText_Solid(font, "QUIT", black);
        character_message = TTF_RenderText_Solid(font, "CHARACTER", black);
    }		

		//Bigger font size for level and end game messages
    font = TTF_OpenFont("Super Mario Bros_.ttf", 50);
    end_message = TTF_RenderText_Solid(font, "YOU HAVE WON!!", white);
		lost_message = TTF_RenderText_Solid(font, "YOU HAVE LOST.", white);
    level1_message = TTF_RenderText_Solid(font, "Level 1", white);
    level2_message = TTF_RenderText_Solid(font, "Level 2",white);
    level3_message = TTF_RenderText_Solid(font, "Level 3", white);
    level4_message = TTF_RenderText_Solid(font, "Level 4", white);
    level5_message = TTF_RenderText_Solid(font, "Final Level: Level 5", white);

		//Character option messages 
    font = TTF_OpenFont("Super Mario Bros_.ttf", 20);
    leprechaun_message = TTF_RenderText_Solid(font, "Leprechaun: Press 1", white);
    Scotty_message = TTF_RenderText_Solid(font, "Prof. Emrich: Press 2", white);

		//Reset message for end of game
    reset_message = TTF_RenderText_Solid(font, "Press 'h' to go to home screen.", white);		


		//Font for the score/lives of player
		font = TTF_OpenFont("Super Mario Bros_.ttf", 35);

		//Converts an int into a string pointer
		stringstream score;
		stringstream lives;
		score << POINTS;
		lives << LIVES;
		string temp_score = score.str();
		string temp_lives = lives.str();
		

		char const* scorePtr = temp_score.c_str();
		char const* livesPtr = temp_lives.c_str();

		//Displays the score on the screen
		score_message = TTF_RenderText_Solid(font, "Score: ", white);
		score_value = TTF_RenderText_Solid(font, scorePtr, white);		

		//Displays the number of lives on the screen
		lives_message = TTF_RenderText_Solid(font, "Lives: ",white);
		lives_value = TTF_RenderText_Solid(font , livesPtr, white);
		
}

//Sets the size and loads the files for the sprite options on the character option screen
void home_screen::set_character_image(Surfaces &surface)
{
    char_option1 = surface.load_files("Leprechaun_Clip.png", SCALE);
    char_option2 = surface.load_files("Scotty_Clip.png", SCALE);
    clips[ 1 ].x = FOO_WIDTH * 6;
    clips[ 1 ].y = 0;
    clips[ 1 ].w = FOO_WIDTH;
    clips[ 1 ].h = FOO_HEIGHT; 
}

//Displays level transition messages. This will also display the lives left and the score
void home_screen::display_next_level(Surfaces &level_screen, int level)
{
    set_messages(0);

    if (level == 1 )
    {
        level_screen.apply_surface(SCREEN_WIDTH/2-110, SCREEN_HEIGHT/2-20, level1_message, screen);
    }
    else if (level == 2)
    {
        level_screen.apply_surface(SCREEN_WIDTH/2-110, SCREEN_HEIGHT/2-20, level2_message, screen);
    }
    else if (level==3)
    {
        level_screen.apply_surface(SCREEN_WIDTH/2-110, SCREEN_HEIGHT/2-20, level3_message, screen);
    }
    else if (level==4)
    {
        level_screen.apply_surface(SCREEN_WIDTH/2-110, SCREEN_HEIGHT/2-20, level4_message, screen);
    }
    else if (level==5)
    {
        level_screen.apply_surface(SCREEN_WIDTH/2-305, SCREEN_HEIGHT/2-20, level5_message, screen);
    }


		level_screen.apply_surface(100,10, score_message, screen);
		level_screen.apply_surface(280, 10, score_value, screen); //Score
		level_screen.apply_surface(550,10, lives_message, screen); //Lives
		level_screen.apply_surface(700, 10, lives_value, screen);

}

//This is the end message of the game if the user completed all 5 levels
void home_screen::end_level(Surfaces &end_screen)
{
    set_messages(0);

		end_screen.apply_surface(100,10, score_message, screen);
		end_screen.apply_surface(280, 10, score_value, screen); //Score
		end_screen.apply_surface(550,10, lives_message, screen); //Lives
		end_screen.apply_surface(700, 10, lives_value, screen);
    end_screen.apply_surface(SCREEN_WIDTH/2-230, SCREEN_HEIGHT/2-50, end_message, screen);
    end_screen.apply_surface(SCREEN_WIDTH/2-200, SCREEN_HEIGHT/2+20, reset_message, screen);
}

//This is the end message of the game if the user lost all 3 lives before completing the game
void home_screen::lost(Surfaces &end_screen)
{
		set_messages(0);

		end_screen.apply_surface(100,10, score_message, screen);
		end_screen.apply_surface(280, 10, score_value, screen); //Score
		end_screen.apply_surface(550,10, lives_message, screen); //Lives
		end_screen.apply_surface(700, 10, lives_value, screen);
   end_screen.apply_surface(SCREEN_WIDTH/2-220, SCREEN_HEIGHT/2-50, lost_message, screen);
    end_screen.apply_surface(SCREEN_WIDTH/2-200, SCREEN_HEIGHT/2+20, reset_message, screen);


}


//Sets display of home/main screen. Will display the title "Super Holy Cross Bros" along with a start, character, and quit option
void home_screen::display_main(Surfaces &main_screen, int button_hover)
{
    set_messages(button_hover);

    main_screen.apply_surface(565, 375, start_message, start_screen_graphic);
    main_screen.apply_surface(575, 412, quit_message, start_screen_graphic);
    main_screen.apply_surface(550, 448, character_message, start_screen_graphic);
    main_screen.apply_surface(0, 0, start_screen_graphic, screen);
}

//Places the character sprites on the screen for the character option screen
void home_screen::display_character(Surfaces &character_screen)
{
    set_messages(0);
    set_character_image(character_screen);

    character_screen.apply_surface(SCREEN_WIDTH/4 - 1.5*FOO_WIDTH-30, SCREEN_HEIGHT/2 + FOO_HEIGHT/2 + 10, leprechaun_message, screen);
    character_screen.apply_surface(SCREEN_WIDTH/4 - FOO_WIDTH/2, SCREEN_HEIGHT/2-FOO_HEIGHT/2, char_option1, screen, &clips[1]);

    character_screen.apply_surface(3*SCREEN_WIDTH/4 - 1.5*FOO_WIDTH-30, SCREEN_HEIGHT/2 + FOO_HEIGHT/2 +10, Scotty_message, screen);
    character_screen.apply_surface(3*SCREEN_WIDTH/4 - FOO_WIDTH/2, SCREEN_HEIGHT/2-FOO_HEIGHT/2, char_option2, screen, &clips[1]);

    SDL_Flip( screen );
}



