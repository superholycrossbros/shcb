/* Natalie Sanders, Katie Weiss, Kendra Bilardello
   transition_screens.h
   Functions for class for the images on the home/main_screen
*/

#include "transition_screens.h"
#include <sstream>
#include <string.h>

using namespace std;

//Constructor will initialized all values to NULL and load the start screen graphic initially
transition_screens::transition_screens ():level_up_sound ("level_start.wav", " "), won_sound("win.wav"," "), lost_sound("lost.wav", " ")
{
  TTF_Init ();

  //Initialize surfaces
  start_screen_graphic = load_files ("start_screen.png", 1);
  start_message = NULL;
  quit_message = NULL;
  character_message = NULL;

  leprechaun_message = NULL;
  Scotty_message = NULL;
  char_option1 = NULL;
  char_option2 = NULL;

  score_message = NULL;
  lives_message = NULL;

  score_value = NULL;
  lives_value = NULL;

  //////////////// CONSTANTS /////////////

  SDL_Color white = { 0xFF, 0xFF, 0xFF };	//Color is white

  //Bigger font size for level and end game messages
  font = TTF_OpenFont ("Super Mario Bros_.ttf", 50);
  end_message = TTF_RenderText_Solid (font, "YOU HAVE WON!!", white);
  lost_message = TTF_RenderText_Solid (font, "YOU HAVE LOST.", white);
  button = NULL;

  level1_message = TTF_RenderText_Solid (font, "Level 1", white);
  level2_message = TTF_RenderText_Solid (font, "Level 2", white);
  level3_message = TTF_RenderText_Solid (font, "Level 3", white);
  level4_message = TTF_RenderText_Solid (font, "Level 4", white);
  level5_message = TTF_RenderText_Solid (font, "Final Level: Level 5", white);

  //Title for character screen
  character_title = TTF_RenderText_Solid (font, "Choose a Character", white);


  //Home Button Clip
  button_clip[0].x = 0;
  button_clip[0].y = 0;
  button_clip[0].w = 300;
  button_clip[0].h = 150;

  button_clip[1].x = 0;
  button_clip[1].y = 150;
  button_clip[1].w = 300;
  button_clip[1].h = 150;

  TTF_CloseFont (font);
}

//Deconstructor will be called when object goes out of scope. It will free all the surfaces at this time. 
transition_screens::~transition_screens ()
{
  SDL_FreeSurface (start_screen_graphic);
  SDL_FreeSurface (character_title);
  SDL_FreeSurface (level1_message);
  SDL_FreeSurface (level2_message);
  SDL_FreeSurface (level3_message);
  SDL_FreeSurface (level4_message);
  SDL_FreeSurface (level5_message);
  SDL_FreeSurface (end_message);
  SDL_FreeSurface (lost_message);

  TTF_Quit ();

  level_up_sound.clean_up ();
  won_sound.clean_up();
  lost_sound.clean_up();
}

//Sets all the messages in one function. Each message will be put on the screen using a "mario" font
void
transition_screens::start_messages (int button_hover)
{
  SDL_Color black = { 0x00, 0x00, 0x00 };	//Color is black
  SDL_Color white = { 0xFF, 0xFF, 0xFF };	//Color is white

  font = TTF_OpenFont ("BAUHS93.TTF", 20);	// Small font size

  // If the mouse is over start or quit or character, that option will be turned to white text
  if (button_hover == 1)
    {
      start_message = TTF_RenderText_Solid (font, "START", white);
      quit_message = TTF_RenderText_Solid (font, "QUIT", black);
      character_message = TTF_RenderText_Solid (font, "CHARACTER", black);
    }
  else if (button_hover == 2)
    {
      start_message = TTF_RenderText_Solid (font, "START", black);
      quit_message = TTF_RenderText_Solid (font, "QUIT", white);
      character_message = TTF_RenderText_Solid (font, "CHARACTER", black);
    }
  else if (button_hover == 3)
    {
      start_message = TTF_RenderText_Solid (font, "START", black);
      quit_message = TTF_RenderText_Solid (font, "QUIT", black);
      character_message = TTF_RenderText_Solid (font, "CHARACTER", white);
    }
  else if (button_hover == 0)
    {
      start_message = TTF_RenderText_Solid (font, "START", black);
      quit_message = TTF_RenderText_Solid (font, "QUIT", black);
      character_message = TTF_RenderText_Solid (font, "CHARACTER", black);
    }

  TTF_CloseFont (font);
}

void
transition_screens::set_pts_lives_message ()
{
  //SDL_FreeSurface(score_message);
  SDL_Color red = { 0x99, 0x66, 0xCC };	//Color is lime

  //Font for the score/lives of player
  font = TTF_OpenFont ("BAUHS93.TTF", 30);

  //Converts an int into a string pointer
  stringstream score;
  stringstream lives;
  score << POINTS;
  lives << LIVES;
  string temp_score = score.str ();
  string temp_lives = lives.str ();

  char const *livesPtr; 
  char const *scorePtr = temp_score.c_str ();
  if(LIVES <= 0) // In case character is hit by two enemies before the screen can refresh, lives cannot be negative
  {
  	livesPtr = "0";
  }
  else
  {
        livesPtr = temp_lives.c_str ();
  }

  //Displays the score on the screen
  score_message = TTF_RenderText_Solid (font, "Score: ", red);
  score_value = TTF_RenderText_Solid (font, scorePtr, red);

  //Displays the number of lives on the screen
  lives_message = TTF_RenderText_Solid (font, "Lives: ", red);
  lives_value = TTF_RenderText_Solid (font, livesPtr, red);

  TTF_CloseFont (font);

}

//Sets the size and loads the files for the sprite options on the character option screen
void
transition_screens::set_character_screen (Surfaces & surface,
					  int button_hover)
{
  SDL_Color blue = { 0x00, 0xFF, 0xFF };	//Color is blue
  SDL_Color white = { 0xFF, 0xFF, 0xFF };	//Color is white

  font = TTF_OpenFont ("BAUHS93.TTF", 20);	// Small font size

  // If the mouse is over start or quit or character, that option will be turned to white text
  if (button_hover == 1)
    {
      leprechaun_message = TTF_RenderText_Solid (font, "Leprechaun", blue);
      Scotty_message = TTF_RenderText_Solid (font, "Prof. Emrich", white);
    }
  else if (button_hover == 2)
    {
      leprechaun_message = TTF_RenderText_Solid (font, "Leprechaun", white);
      Scotty_message = TTF_RenderText_Solid (font, "Prof. Emrich", blue);
    }
  else if (button_hover == 0)
    {
      leprechaun_message = TTF_RenderText_Solid (font, "Leprechaun", white);
      Scotty_message = TTF_RenderText_Solid (font, "Prof. Emrich", white);
    }

  TTF_CloseFont (font);

  clips[1].x = FOO_WIDTH / SCALE * 6;  
  clips[1].y = 0;
  clips[1].w = FOO_WIDTH / SCALE;
  clips[1].h = FOO_HEIGHT / SCALE;
  char_option1 = surface.load_files ("Leprechaun_Clip.png", 1);
  char_option2 = surface.load_files ("Scotty_Clip.png", 1);
}

void
transition_screens::display_points_lives (Surfaces & current_surface)
{
  set_pts_lives_message ();

  current_surface.apply_surface (20,  10, score_message, screen);
  current_surface.apply_surface (140, 10, score_value, screen);	// score_message
  current_surface.apply_surface (645, 10, lives_message, screen);// Lives
  current_surface.apply_surface (745, 10, lives_value, screen);

  SDL_Flip( screen );

  SDL_FreeSurface(score_message);
  SDL_FreeSurface(score_value);
  SDL_FreeSurface(lives_message);
  SDL_FreeSurface(lives_value);
}

  

//Displays level transition messages. This will also display the lives left and the score
void transition_screens::display_next_level (Surfaces & level_surface,
					       int level)
  {
    if (level == 1)
      {
	level_surface.apply_surface (SCREEN_WIDTH / 2 - 110,
				    SCREEN_HEIGHT / 2 - 20, level1_message,
				    screen);
      }
    else if (level == 2)
      {
	level_surface.apply_surface (SCREEN_WIDTH / 2 - 110,
				    SCREEN_HEIGHT / 2 - 20, level2_message,
				    screen);
      }
    else if (level == 3)
      {
	level_surface.apply_surface (SCREEN_WIDTH / 2 - 110,
				    SCREEN_HEIGHT / 2 - 20, level3_message,
				    screen);
      }
    else if (level == 4)
      {
	level_surface.apply_surface (SCREEN_WIDTH / 2 - 110,
				    SCREEN_HEIGHT / 2 - 20, level4_message,
				    screen);
      }
    else if (level == 5)
      {
	level_surface.apply_surface (SCREEN_WIDTH / 2 - 305,
				    SCREEN_HEIGHT / 2 - 20, level5_message,
				    screen);
      }

    display_points_lives(level_surface);

    level_up_sound.play ();

  }

void transition_screens::home_button(int button_hover, Surfaces & end_screen)
{
  float scale = 0.6;

  //Home Button
  button_clip[0].x = 0;
  button_clip[0].y = 0;
  button_clip[0].w = 300*scale;
  button_clip[0].h = 150*scale;

  button_clip[1].x = 0;
  button_clip[1].y = 150*scale;
  button_clip[1].w = 300*scale;
  button_clip[1].h = 150*scale;

    button = end_screen.load_files ("button_clip.png", scale);

    if (button_hover)
    {
        end_screen.apply_surface (SCREEN_WIDTH/2 - button_clip[1].w/2, SCREEN_HEIGHT/2 + 20, button, screen, &button_clip[1]);
    }
    else
    {
        end_screen.apply_surface (SCREEN_WIDTH/2 - button_clip[0].w/2, SCREEN_HEIGHT/2 + 20, button, screen, &button_clip[0]);
    }

    SDL_FreeSurface(button);
}
      
SDL_Rect transition_screens::getButton()
{
  SDL_Rect temp;
  temp.x = SCREEN_WIDTH/2 - button_clip[0].w/2;
  temp.y = SCREEN_HEIGHT/2 + 20;
  temp.w = button_clip[0].w;
  temp.h = button_clip[0].h;
  return temp;
}
   
//This is the end message of the game if the user completed all 5 levels
void transition_screens::end_level (Surfaces & end_screen, int button_hover)
  {
    display_points_lives(end_screen);
    home_button(button_hover, end_screen);

    end_screen.apply_surface (SCREEN_WIDTH / 2 - 230, SCREEN_HEIGHT / 2 - 50, end_message, screen);

    SDL_Flip ( screen );

    // play winning sound once, even though function is called more than once
    static int play = 0;
    if(play == 0) won_sound.play();

    play ++;

 
   }

//This is the end message of the game if the user lost all 3 lives before completing the game
void transition_screens::lost (Surfaces & end_screen, int button_hover)
  {
    display_points_lives(end_screen);
    home_button(button_hover, end_screen);

    end_screen.apply_surface (SCREEN_WIDTH / 2 - 220, SCREEN_HEIGHT / 2 - 50,
			      lost_message, screen);
    SDL_Flip ( screen );

    // play losing sound once, even though function is called more than once
    static int play = 0;
    if(play == 0) lost_sound.play();

    play++;

  }


//Sets display of home/main screen. Will display the title "Super Holy Cross Bros" along with a start, character, and quit option
  void transition_screens::display_main (Surfaces & main_screen,
					 int button_hover)
  {
    start_messages (button_hover);

    main_screen.apply_surface (570, 375, start_message, start_screen_graphic);
    main_screen.apply_surface (575, 412, quit_message, start_screen_graphic);
    main_screen.apply_surface (550, 448, character_message, start_screen_graphic);
    main_screen.apply_surface (0, 0, start_screen_graphic, screen);

    SDL_FreeSurface(start_message);
    SDL_FreeSurface(quit_message);
    SDL_FreeSurface(character_message);
    
    SDL_Flip (screen);
  }

//Places the character sprites on the screen for the character option screen
  void transition_screens::display_character (Surfaces & character_screen,
					      int button_hover)
  {
    set_character_screen (character_screen, button_hover);

    SDL_FillRect (screen, NULL, 0x000000);

    character_screen.apply_surface (SCREEN_WIDTH / 2 - 300, 50, character_title, screen);

    character_screen.apply_surface (SCREEN_WIDTH/4-1.5*FOO_WIDTH-30, SCREEN_HEIGHT/2+FOO_HEIGHT/2+10, leprechaun_message, screen);
    character_screen.apply_surface (SCREEN_WIDTH/4-FOO_WIDTH/2, SCREEN_HEIGHT/2-FOO_HEIGHT/2, char_option1, screen, &clips[1]);

    character_screen.apply_surface (3*SCREEN_WIDTH/4-1.5*FOO_WIDTH-30, SCREEN_HEIGHT / 2 + FOO_HEIGHT / 2 + 10, Scotty_message, screen);
    character_screen.apply_surface (3*SCREEN_WIDTH/4-FOO_WIDTH/2, SCREEN_HEIGHT/2-FOO_HEIGHT/2, char_option2, screen, &clips[1]);

    SDL_FreeSurface(char_option1);
    SDL_FreeSurface(char_option2);
    SDL_FreeSurface(leprechaun_message);
    SDL_FreeSurface(Scotty_message);
    SDL_Flip (screen);
  }
