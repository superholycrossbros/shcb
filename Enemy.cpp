/* Natalie Sanders, Kendra Bilardello, Katie Weiss
   Enemy.h
   Creates, moves, displays, and detects collisions for enemies
*/

#include "Enemy.h"
#include "surfaces.h"
#include <math.h>
using namespace std;

//Define constructor that will read in the enemy file and put the values into a vector of SDL_Rects and a vector of SDL_Surfaces
Enemy::Enemy(char *file_name) 
{
    FILE *f = fopen(file_name, "r"); // Opens the input file and creates a file pointer
    if(!f) // Prints error message if the file could not be opened
    {
        cout << "Could not open file" <<endl;
    }
    else
    {

        char line[100];
        int j = 0;
        while(fgets(line, sizeof(line), f)) // Takes one line from the file at a time
        {
	    // Creates temporary variable to hold current lines data (coordinates of enemy)
            SDL_Rect temp;
            temp.x = atoi(line);
            temp.y = atoi(&line[5]);
            temp.w = FOO_WIDTH;
            temp.h = FOO_HEIGHT;
            ENEMIES.push_back(temp); // Puts line into vector of SDL_Rects
        }
        fclose(f);
    }  


    TOTAL_ENEMIES= ENEMIES.size(); // Tracks the number of enemies on the screen


    // Initializing vector of SDL_Surface pointers to display trojan enemies
    for(int i = 0; i < TOTAL_ENEMIES; i++)
    {
        SDL_Surface *temp = NULL;
        temp = load_files("Trojan_Clip.png", SCALE);			
        if(temp != NULL)
        {
            enemySheet.push_back(temp);		
        }
        
    }

    // Initialize puff when the enemy is destroyed
    puff = load_files("puff.png",SCALE);

    // Set clips for trojan image
    set_clips();

    frame  = 0;
    status = 0;
    xVel   = 0;
    theta  = 0;
    apply_puff =0;

		
    
}

//Loops through the vector of SDL_Surfaces and frees them
Enemy::~Enemy() 
{
	for (int i=0; i<enemySheet.size(); i++){
		SDL_FreeSurface(enemySheet[i]);
	}
	
        SDL_FreeSurface(puff);

}

void Enemy::move()
{
    // Calculating the velocity using sin() creates an oscillating motion such that the enemy paces back and forth
    int prev_xVel = xVel;
    xVel      = 100*sin(theta);	
    theta    += 0.1; //Incrememnt position of enemy

    if( x+xVel < x+prev_xVel ) //If character is moving right
    {
        //Set the animation to left
        status = FOO_LEFT;

        //Move to the next frame in the animation
        frame++;
    }

    else if( x+xVel > x+prev_xVel ) //If character is moving left
    {
        //Set the animation to right
        status = FOO_RIGHT;

        //Move to the next frame in the animation
        frame++;
    } 

}

void Enemy::reset_positions()
{
    xVel  = 0;
    theta = 0;
}

// Displays the walking Trojan enemy. This function takes in an x value, an add and an add_prev variable. The x is the position of the main_character. The add and the add_prev are the values to determine the next location of the Trojan enemy and the direction he should be facing. If the add is greater than the previous add value (add_pre), then the Trojan is moving left and if the add is less than the previous add values, the Trojan is moving right.

void Enemy::apply_enemy(int x)
{
    // Loop the animation (go though the stages of walking)
    if( frame >= 5 ){
        frame = 0;
    }

    //Apply the correct image of the character to the screen
    if( status == FOO_RIGHT ) //Facing to the right
    {			
	for (int i=0; i<enemySheet.size(); i++) // Loops through vector of enemies to display all
	{	
        	apply_surface( ENEMIES[i].x+xVel- camera.x-x, ENEMIES[i].y-camera.y, enemySheet[i], screen, &EnemyclipsRight[frame]); 
     	}
    }
    else if( status == FOO_LEFT ) //Facing to the left
    {		
	for (int i=0; i<enemySheet.size(); i++) // Loops through enemy vector to display all
	{	
        	apply_surface( ENEMIES[i].x+xVel- camera.x-x, ENEMIES[i].y-camera.y, enemySheet[i], screen, &EnemyclipsLeft[frame]); 
	}
    }
		

    //If an enemy was erased, apply_puff=1 and puff image will be displayed where enemy was
    if (apply_puff == 1)
    {
	apply_surface(puff_coord.x, puff_coord.y, puff, screen);
    }

    if (TIME<time_puff-1) //Sets length of time to display the puff
    {
	apply_puff = 0;
    }


}

//Returns the the coordinates of enemy i
SDL_Rect Enemy::getEnemyRect(int i)
{
	return ENEMIES[i];
}


//Clips the Trojan character image
void Enemy::set_clips()  
{
    //Character walking to the right
    EnemyclipsRight[ 0 ].x = 0;
    EnemyclipsRight[ 0 ].y = 0;
    EnemyclipsRight[ 0 ].w = FOO_WIDTH;
    EnemyclipsRight[ 0 ].h = FOO_HEIGHT;

    EnemyclipsRight[ 1 ].x = FOO_WIDTH;
    EnemyclipsRight[ 1 ].y = 0;
    EnemyclipsRight[ 1 ].w = FOO_WIDTH;
    EnemyclipsRight[ 1 ].h = FOO_HEIGHT;

    EnemyclipsRight[ 2 ].x = FOO_WIDTH * 2;
    EnemyclipsRight[ 2 ].y = 0;
    EnemyclipsRight[ 2 ].w = FOO_WIDTH;
    EnemyclipsRight[ 2 ].h = FOO_HEIGHT;

    EnemyclipsRight[ 3 ].x = FOO_WIDTH * 3;
    EnemyclipsRight[ 3 ].y = 0;
    EnemyclipsRight[ 3 ].w = FOO_WIDTH;
    EnemyclipsRight[ 3 ].h = FOO_HEIGHT;

    EnemyclipsRight[ 4 ].x = FOO_WIDTH * 4;
    EnemyclipsRight[ 4 ].y = 0;
    EnemyclipsRight[ 4 ].w = FOO_WIDTH;
    EnemyclipsRight[ 4 ].h = FOO_HEIGHT;

    EnemyclipsRight[ 5 ].x = FOO_WIDTH * 5;
    EnemyclipsRight[ 5 ].y = 0;
    EnemyclipsRight[ 5 ].w = FOO_WIDTH;
    EnemyclipsRight[ 5 ].h = FOO_HEIGHT; 


    //Character walking to the left
    EnemyclipsLeft[ 0 ].x = 0;
    EnemyclipsLeft[ 0 ].y = FOO_HEIGHT;
    EnemyclipsLeft[ 0 ].w = FOO_WIDTH;
    EnemyclipsLeft[ 0 ].h = FOO_HEIGHT;

    EnemyclipsLeft[ 1 ].x = FOO_WIDTH;
    EnemyclipsLeft[ 1 ].y = FOO_HEIGHT;
    EnemyclipsLeft[ 1 ].w = FOO_WIDTH;
    EnemyclipsLeft[ 1 ].h = FOO_HEIGHT;

    EnemyclipsLeft[ 2 ].x = FOO_WIDTH * 2;
    EnemyclipsLeft[ 2 ].y = FOO_HEIGHT;
    EnemyclipsLeft[ 2 ].w = FOO_WIDTH;
    EnemyclipsLeft[ 2 ].h = FOO_HEIGHT;

    EnemyclipsLeft[ 3 ].x = FOO_WIDTH * 3;
    EnemyclipsLeft[ 3 ].y = FOO_HEIGHT;
    EnemyclipsLeft[ 3 ].w = FOO_WIDTH;
    EnemyclipsLeft[ 3 ].h = FOO_HEIGHT;

    EnemyclipsLeft[ 4 ].x = FOO_WIDTH * 4;
    EnemyclipsLeft[ 4 ].y = FOO_HEIGHT;
    EnemyclipsLeft[ 4 ].w = FOO_WIDTH;
    EnemyclipsLeft[ 4 ].h = FOO_HEIGHT;

    EnemyclipsLeft[ 5 ].x = FOO_WIDTH * 5;
    EnemyclipsLeft[ 5 ].y = FOO_HEIGHT;
    EnemyclipsLeft[ 5 ].w = FOO_WIDTH;
    EnemyclipsLeft[ 5 ].h = FOO_HEIGHT;


}


bool Enemy::check_collision(SDL_Rect coord)
{
    for (int i=0; i<TOTAL_ENEMIES; i++)
    {
        //Character jumped on enemy, need to set the position for puff
        if (check_top(ENEMIES[i], coord))
        {
		//Sets data to display the puff after enemy destroyed
		apply_puff=1;
		puff_coord.x=ENEMIES[i].x-camera.x-coord.x+xVel;
		puff_coord.y=ENEMIES[i].y-camera.y;
		time_puff=TIME;

		//Remove enemy from SDL_Surface vector and SDL_Rect vector
		SDL_FreeSurface(enemySheet[i]);
		enemySheet.erase(enemySheet.begin()+i);
		ENEMIES.erase(ENEMIES.begin()+i);

		POINTS+=100; //100 pts for killing enemy
		TOTAL_ENEMIES--;
		return true;
					
        }
        //Character ran into/was run into by enemy
        else if (check_sides(ENEMIES[i], coord))
        {
		//Erases the enemy from vectors so it won't remove 2 lives
		//(because of rechecking if run into same enemy)
		SDL_FreeSurface(enemySheet[i]);
		enemySheet.erase(enemySheet.begin()+i);
		ENEMIES.erase(ENEMIES.begin()+i);

		TOTAL_ENEMIES--;
		LIVES--;
					
        }
    }

    return false;
}

//A special collision detection that helps regulate the lives of the main_character. If the main_character runs into the right or left of the Trojan, he will die and this funciton will return true.
bool Enemy::check_sides(SDL_Rect B, SDL_Rect coord)			
{
    int leftA, leftB;
    int rightA, rightB;
    int topA, topB;
    int bottomA, bottomB;

    //Calculate the sides of Rect A, the character
    leftA = coord.x-camera.x+10;
    rightA = coord.x + FOO_WIDTH-camera.x-25;
    topA = coord.y-camera.y+10;
    bottomA = coord.y + FOO_HEIGHT-camera.y;

    //Calculate the sides of Rect B, the enemy
    leftB = B.x+xVel-camera.x-coord.x;
    rightB = B.x +xVel + B.w-camera.x-coord.x;
    topB = B.y-camera.y;
    bottomB = B.y + B.h-camera.y;
   
		
    if( rightA>=leftB && rightA<=leftB+10 && bottomA>=topB && topA<=bottomB)
    {
        return true; //Right of character overlaps with Trojan

    }
    else if ( leftA<=rightB && leftA>=rightB-10 && bottomA>=topB && topA<=bottomB)
    {
	return true; //Left of character overlaps with Trojan
    }

   
    return false; //No part of the character overlapped
}


//Collision detection for the top of the enemy to determine if the main_character killed the Trojan
bool Enemy::check_top(SDL_Rect B, SDL_Rect coord) 			
{
		int leftA, leftB;
    int rightA, rightB;
    int topA, topB;
    int bottomA, bottomB;

    //Calculate the sides of Rect A, the character
    leftA = coord.x-camera.x+10;
    rightA = coord.x + FOO_WIDTH-camera.x-25;
    topA = coord.y-camera.y+10;
    bottomA = coord.y + FOO_HEIGHT-camera.y;

    //Calculate the sides of Rect B, the enemy
    leftB = B.x+xVel -camera.x-coord.x;
    rightB = B.x +xVel +B.w-camera.x-coord.x;
    topB = B.y-camera.y;
    bottomB = B.y + B.h-camera.y;

		// Top of enemy overlaps with bottom of character
    if(bottomA <= topB+15 && bottomA >= topB && leftA <= rightB && rightA >= leftB) 
    {
        return true; //Enemy was killed and should be removed from screen
    }


    return false; //The main_character did not land on top of enemy

}
