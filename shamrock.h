/* Natalie Sanders, Kendra Bilardello, Katie Weiss
   shamrock.h
   Interface for shamrock class which displays shamrocks and detects character collisions with shamrocks
*/

#ifndef SHAMROCK_H
#define SHAMROCK_H

#include "constants.h"
#include "surfaces.h"
#include <vector>

extern SDL_Surface *screen;
extern SDL_Surface *background;
extern SDL_Rect camera;
extern int TOTAL_SHAMROCKS;
extern double POINTS;
extern int TIME;
extern int LIVES;

class Shamrock : public Surfaces
{
    public:
        Shamrock(char *);
        ~Shamrock();
        void apply_shamrock(int); 				// applies shamrock surfaces to the screen
        void check_collision(SDL_Rect, vector <SDL_Rect>);	// checks if character has come across a shamrock

        vector <SDL_Rect> SHAMROCKS; 				// holds positions of the shamrocks
        vector <SDL_Surface *> shamrockSheet; 			// holds the surfaces of the shamrocks

	SDL_Surface *puff; 					// The surface of the star which appears after a shamrock's collected
	SDL_Rect puff_coord;					// Coordinates of the current puff, if any
	int apply_puff;						// Indicates puff should be applied
	int time_puff;						// Time puff was applied; used to show the image for a limited period

	int collected;						// How many shamrocks collected
	SDL_Surface *up;					// The surface for the 1up star which appears after 7 shamrocks are collected
	SDL_Rect up_coord;					// Coordinates of the current 1up star, if any
	int apply_1up;						// Indicates 1up star should be applied;
	int time_1up;						// Time 1up star was applied; used to show the image for a limited period
};

#endif
