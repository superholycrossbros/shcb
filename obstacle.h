/* Natalie Sanders, Katie Weiss, Kendra Bilardello
   obstacles.h
   Class for creating obstacles. Tile and enemy will inherit from this
*/

#ifndef OBSTACLES_H
#define OBSTACLES_H

#include "surfaces.h"
#include "constants.h"

using namespace std;

extern SDL_Surface *screen;
extern SDL_Surface *background;
extern SDL_Rect camera;
//extern int TOTAL_TILES;

class obstacle : public Surfaces
{
	public:
		obstacle();
		~obstacle();
		virtual void apply_obstacle_surface() = 0;
		virtual void set_clips() = 0;
		virtual SDL_Rect get_dim() = 0; //returns dimensions around obstacle

	private:






}





#endif
