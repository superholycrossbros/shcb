all:	main

main: main.o timer.o main_character.o surfaces.o tile.o transition_screens.o Enemy.o boss.o shamrock.o Sound.o
	g++ main.o timer.o main_character.o surfaces.o tile.o transition_screens.o Enemy.o boss.o shamrock.o Sound.o -g -lSDL -lSDL_image -lSDL_ttf -lSDL_gfx -lSDL_mixer -o main

main_character.o:  main_character.cpp main_character.h surfaces.h
	g++ -c main_character.cpp -g -lSDL -lSDL_image -lSDL_ttf -lSDL_gfx -lSDL_mixer

tile.o: tile.cpp tile.h surfaces.h
	g++ -c tile.cpp -g -lSDL -lSDL_image -lSDL_ttf -lSDL_gfx -lSDL_mixer

timer.o:	timer.cpp
	g++ -c timer.cpp -g -lSDL -lSDL_image -lSDL_ttf -lSDL_gfx -lSDL_mixer

surfaces.o: surfaces.cpp surfaces.h main_character.h
	g++ -c surfaces.cpp -g -lSDL -lSDL_image -lSDL_ttf -lSDL_gfx -lSDL_mixer

transition_screens.o: transition_screens.cpp transition_screens.h surfaces.h
	g++ -c transition_screens.cpp -g -lSDL -lSDL_image -lSDL_ttf -lSDL_gfx -lSDL_mixer

Enemy.o: Enemy.cpp Enemy.h surfaces.h
	g++ -c Enemy.cpp -g -lSDL -lSDL_image -lSDL_ttf -lSDL_gfx -lSDL_mixer

shamrock.o: shamrock.cpp shamrock.h surfaces.h
	g++ -c shamrock.cpp -g -lSDL -lSDL_image -lSDL_ttf -lSDL_gfx -lSDL_mixer

boss.o : boss.cpp boss.h surfaces.h
	g++ -c boss.cpp -g -lSDL -lSDL_image -lSDL_ttf -lSDL_gfx -lSDL_mixer

Sound.o: Sound.cpp Sound.h
	g++ -c Sound.cpp -g -lSDL -lSDL_image -lSDL_ttf -lSDL_gfx -lSDL_mixer

main.o: main.cpp
	g++ -c main.cpp -g -lSDL -lSDL_image -lSDL_ttf -lSDL_gfx -lSDL_mixer

clean:
	rm -f *.o main
