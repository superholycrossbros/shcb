/* Natalie Sanders, Katie Weiss, Kendra Bilardello
   timer.h
   Class for keeping track of time throughout the game
*/

#ifndef TIMER_H
#define TIMER_H

#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include <string>
#include <iostream>

//The timer
class Timer
{
   public:
      Timer(); //Initalize start ticks and started variable
      void start(); //Starts the timer
      double get_ticks(); //Returns current ticks-start ticks
      bool is_started(); //Returns whether timer is started

   private:
      int startTicks; //Clock time when clock starts
      bool started; //Is timer started?

};

#endif
