/* Natalie Sanders, Katie Weiss, Kendra Bilardello
   timer.cpp
   Class for keeping track of time throughout the game
*/

#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include <string>
#include <iostream>
#include "timer.h"

//Constructor for the timer that will initalize the variables
Timer::Timer()
{
    //Initialize the variables
    startTicks = 0;
    started = false;
}

//Starts the timer
void Timer::start()
{
    //Start the timer
    started = true;

    //Get the current clock time
    startTicks = SDL_GetTicks();
}

//Returns the amount of time that the program has been running
double Timer::get_ticks()
{
    //If the timer is running
    if( started == true )
    {
        //Return the current time minus the start time
        return SDL_GetTicks() - startTicks;

    }

    //If the timer isn't running
    return 0;
}

//Returns true or false depending on whether the timer is started or not
bool Timer::is_started()
{
    return started;
}



