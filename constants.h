/* Natalie Sanders, Katie Weiss, Kendra Bilardello
   constants.h
   Contains global parameters for the screen, the size of the character, and the frames per second
*/

#ifndef CONSTANTS_H
#define CONSTANTS_H

#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include <string>
#include <iostream> 

//SCREEN

//Screen attributes
const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 480;
const int SCREEN_BPP = 32;


//LEVELS
//The dimensions of the level
const int LEVEL_WIDTH = 1280*3;
const int LEVEL_HEIGHT = 960;


//FRAMES
//The frames per second
const int FRAMES_PER_SECOND = 10;


//CHARACTER
//The dimensions of the main character
const float SCALE = 0.6;
const float FOO_WIDTH = 99*SCALE;
const float FOO_HEIGHT = 120*SCALE;

//The direction status of the character
const int FOO_RIGHT = 0;
const int FOO_LEFT = 1;


//TILES
//Mapping Tiles
const int TILE_WIDTH=40;
const int TILE_HEIGHT=40;
const int TILE_RED =0;
const int TILE_SPRITES = 1;


//SHAMROCKS
const int SHAM_WIDTH  = 30;
const int SHAM_HEIGHT = 30;




#endif

