//Katie Weiss
//CSE 20212 - Fundamentals of Computing II
//Lab 7/8 - deliverable

/* the purpose of this code is to practice what I have learned
   in the sdltutorials.com and lazyfoo tutorials. In this test,
   I am trying to get an image to displayed on a screen. */



//Follow Lazyfoo tutorial model for optimized screen display


//Necessary headers

#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include <string>


// initialize the screen

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;
const int SCREEN_BPP = 32;


// initialize surfaces to be used in the display
SDL_Surface *background = NULL;
SDL_Surface *screen = NULL;


// Read in file and optimize image to be displayed

SDL_Surface *load_image( std::string filename )
{
   // Temporary storage for image
   SDL_Surface* loadedImage = NULL;

   // Storage for optimized image
   SDL_Surface* optimizeImage = NULL;

   
   // load the image into image storage
   loadedImage = IMG_Load( filename.c_str() );

   //optimize the image if the image is loaded properly
   if( loadedImage != NULL )
   {
	// optimize the image and place in optimizeImage
        optimizeImage = SDL_DisplayFormat( loadedImage );
 
	// free the original image storage
  	SDL_FreeSurface( loadedImage );
    }


    //Return this optimized image
    return optimizeImage;
}




// Function to actually apply the surface

void apply_surface( int x, int y, SDL_Surface* source, SDL_Surface* destination)
{
   // Create a rectangle that will hold the offsets
   SDL_Rect offset;


   // give the offsets to the rectangle
   offset.x = x;
   offset.y = y;


   // Blit the surface
   SDL_BlitSurface(source, NULL, destination, &offset);
}



// Begin main function


int main( int argc, char* args[] )
{
   // Initialize all of the SDL subsystems
   if( SDL_Init( SDL_INIT_EVERYTHING == -1) )
   {
	return 1;
   }    


   // Set up the screen
   screen = SDL_SetVideoMode( SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BPP, SDL_SWSURFACE );


   // if the screen was not set up properly, end the program
   if( screen == NULL )
   {
	return 1;
   }


   // Set a caption for the window
   SDL_WM_SetCaption( "Ocean Paradise", NULL );

   // read in the background image
   background = load_image( "underwater.jpg" );

   //Apply the background to the screen
   apply_surface(180, 140, background, screen);



   // Update the screen
   if(SDL_Flip( screen ) == -1)
   {
    	return 1;
   }



   //Wait 2 seconds
   SDL_Delay( 2000 );

   //Free the background surface
   SDL_FreeSurface( background );

   //Quit SDL
   SDL_Quit();

   return 0;
}





